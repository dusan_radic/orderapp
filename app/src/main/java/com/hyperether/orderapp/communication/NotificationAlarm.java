package com.hyperether.orderapp.communication;

import android.annotation.SuppressLint;
import android.app.ActivityManager;
import android.app.ActivityManager.RunningServiceInfo;
import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.os.PowerManager;
import android.util.Log;

import com.hyperether.orderapp.utils.Preferences;

public class NotificationAlarm extends BroadcastReceiver {
	private static int REFRESH_SECONDS = 10;
	@SuppressLint("Wakelock")
	@Override
	public void onReceive(Context context, Intent intent) {
		PowerManager pm = (PowerManager) context.getSystemService(Context.POWER_SERVICE);
		PowerManager.WakeLock wl = pm.newWakeLock(PowerManager.PARTIAL_WAKE_LOCK, "NotificationAlarm");
		wl.acquire();
		if(isNotificationServiceRunning(context)){
			Log.i(Preferences.LOG_TAG, "NotificationService running");
			//Toast.makeText(context, "NotificationService running", Toast.LENGTH_SHORT).show();
		}else{
			Log.i(Preferences.LOG_TAG, "Starting NotificationService");
			//Toast.makeText(context, "Starting NotificationService", Toast.LENGTH_SHORT).show();
			Intent mServiceIntent = new Intent(context, NotificationService.class);
			context.startService(mServiceIntent);
		}
		wl.release();
	}

	public void SetAlarm(Context context) {
		AlarmManager am = (AlarmManager)context.getSystemService(Context.ALARM_SERVICE);
		Intent i = new Intent(context, NotificationAlarm.class);
		PendingIntent pi = PendingIntent.getBroadcast(context, 0, i, PendingIntent.FLAG_UPDATE_CURRENT);
		am.setRepeating(AlarmManager.RTC_WAKEUP, System.currentTimeMillis(), 1000 * REFRESH_SECONDS, pi); // Millisec * Second * Minute
	}

	public void CancelAlarm(Context context) {
		Intent intent = new Intent(context, NotificationAlarm.class);
		PendingIntent sender = PendingIntent.getBroadcast(context, 0, intent, PendingIntent.FLAG_UPDATE_CURRENT);
		AlarmManager alarmManager = (AlarmManager) context.getSystemService(Context.ALARM_SERVICE);
		alarmManager.cancel(sender);
	}

	private boolean isNotificationServiceRunning(Context context) {
		ActivityManager manager = (ActivityManager) context.getSystemService(Context.ACTIVITY_SERVICE);
		for (RunningServiceInfo service : manager.getRunningServices(Integer.MAX_VALUE)) {
			if (NotificationService.class.getName().equals(service.service.getClassName())) {
				return true;
			}
		}
		return false;
	}
}