package com.hyperether.orderapp.communication;

import org.json.JSONObject;

import android.app.Activity;
import android.app.IntentService;
import android.content.Intent;
import android.util.SparseArray;

public class MenuUpdateService extends IntentService {
	private int result = Activity.RESULT_CANCELED;
	public static final String MENU_ID = "menu_id";
	public static final String RESULT = "result";
	public static final String NOTIFICATION = "com.hyperether.orderapp.communication.menuupdateservice";

	public MenuUpdateService(String name) {
		super(name);
	}

	public MenuUpdateService() {
		super("MenuUpdateService");
	}

	@Override
	protected void onHandleIntent(Intent intent) {
		// Gets data from the incoming Intent
		String mMenuId = intent.getStringExtra(MENU_ID);
		SparseArray<JSONObject> apiResult = ApiCalls.getMenuItems(mMenuId);
		if(apiResult.size() > 0){
			MenuUpdate mMenuUpdate = new MenuUpdate(getApplicationContext());
			mMenuUpdate.updateMenu(apiResult);
			result = Activity.RESULT_OK;
		}
		publishResults(result);
	}

	private void publishResults(int result) {
		Intent intent = new Intent(NOTIFICATION);
		intent.putExtra(RESULT, result);
		sendBroadcast(intent);
	}
}