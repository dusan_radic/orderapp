package com.hyperether.orderapp.communication;

import android.content.Context;
import android.content.Intent;
import android.widget.Toast;

import com.hyperether.orderapp.R;
import com.hyperether.orderapp.components.DataCache;
import com.hyperether.orderapp.components.Order;

/**
 * Class SenderApi encapsulates methods for sending SMS, Email and Web messages
 * @authors Dusan Radic
 * 			Slobodan Prijic
 * @version 1.1, 2014-04-09
 */

public class SenderApi{
	Context context;

	// Get the activity context
	public SenderApi(Context context) {
		this.context = context;
	}

	public void sendSMS(String message) {
		Intent smsIntent = new Intent(Intent.ACTION_VIEW);
		smsIntent.setType("vnd.android-dir/mms-sms");
		smsIntent.putExtra("address", context.getString(R.string.phone));
		smsIntent.putExtra("sms_body", message);
		context.startActivity(smsIntent);
	}

	/**
	 * Method for sending and Email
	 * 
	 * @param message - message text that is to be sent
	 */
	public void sendEmail(String message) {
		Intent i = new Intent(Intent.ACTION_SEND);
		i.setType("message/rfc822");
		String[] email = new String[1];
		email[0] = context.getString(R.string.email);
		i.putExtra(Intent.EXTRA_EMAIL, email);
		if(DataCache.getInstance().getOrder().getOrderType() == Order.ORDER_RESERVATION)
			i.putExtra(Intent.EXTRA_SUBJECT, context.getString(R.string.text_reservation));
		else
			i.putExtra(Intent.EXTRA_SUBJECT, context.getString(R.string.text_order));
		i.putExtra(Intent.EXTRA_TEXT, message);
		try {
			context.startActivity(Intent.createChooser(i, context.getString(R.string.text_send_email)));
		} catch (android.content.ActivityNotFoundException ex) {
			Toast.makeText(context, context.getString(R.string.text_no_email_clients), Toast.LENGTH_SHORT).show();
		}
	}
	/**
	 * Call @OrderInsertService which will send order from DataCache on web server
	 */
	public void startInsertService(int mSendingType){
		Intent mServiceIntent = new Intent(context, OrderInsertService.class);
		mServiceIntent.putExtra(OrderInsertService.SEND_TYPE, mSendingType);
		context.startService(mServiceIntent);
	}
}
