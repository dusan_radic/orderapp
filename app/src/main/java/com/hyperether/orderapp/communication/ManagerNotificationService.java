package com.hyperether.orderapp.communication;

import java.text.SimpleDateFormat;

import org.json.JSONArray;
import org.json.JSONObject;

import android.annotation.SuppressLint;
import android.app.IntentService;
import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.support.v4.app.NotificationCompat;
import android.telephony.TelephonyManager;
import android.util.SparseArray;

import com.hyperether.orderapp.R;
import com.hyperether.orderapp.components.DataCache;
import com.hyperether.orderapp.components.DatabaseHelper;
import com.hyperether.orderapp.components.Food;
import com.hyperether.orderapp.components.Order;
import com.hyperether.orderapp.manager.ManagerOrdersPendingList;

/**
 * This {@link IntentService} communicates with server
 * and retrieves list of orders for confirmation
 * @authors Nebojsa Brankovic
 * 			Slobodan Prijic
 * @version 1.1, 2014-04-09
 */
public class ManagerNotificationService extends IntentService{

	public static final int NOTIFICATION_ID = 6894;

	public ManagerNotificationService(String name) {
		super(name);
	}

	public ManagerNotificationService() {
		super("ManagerNotificationService");
	}

	@Override
	protected void onHandleIntent(Intent intent) {
		TelephonyManager tm = (TelephonyManager) getSystemService(Context.TELEPHONY_SERVICE);
		String imei = (tm.getDeviceId() != null) ? tm.getDeviceId() : android.provider.Settings.Secure.ANDROID_ID;
		JSONObject jSONObject = ApiCalls.getOrderForManager(imei);

		SparseArray<Order> orders = convertFromJSONInOrder(jSONObject);
		if(orders != null && orders.size() > 0) {
			DataCache.getInstance().setOrdersForConfirm(orders);
			notifyManager(orders.size());
		} else {
			notificationCancel();
		}
	}

	private void notifyManager(int size) {
		NotificationManager mNotificationManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);

		NotificationCompat.Builder builder = new NotificationCompat.Builder(
				this).setSmallIcon(R.drawable.ic_order_notify)
				.setContentTitle(getString(R.string.text_logo_name))
				.setContentText(size + " Orders")
				.setAutoCancel(true)
				.setDefaults(Notification.DEFAULT_SOUND);

		/*
		 * Clicking the notification itself displays ManagerOrdersPendingList
		 */
		Intent notificationIntent = new Intent(this,
				ManagerOrdersPendingList.class);
		PendingIntent pendingIntent = PendingIntent.getActivity(this, 2,
				notificationIntent, PendingIntent.FLAG_CANCEL_CURRENT);
		builder.setContentIntent(pendingIntent);

		mNotificationManager.notify(NOTIFICATION_ID, builder.build());
	}

	private void notificationCancel(){
		NotificationManager mNotificationManager = (NotificationManager) this
				.getSystemService(Context.NOTIFICATION_SERVICE);
		mNotificationManager.cancel(NOTIFICATION_ID);
	}

	@SuppressLint("SimpleDateFormat")
	private SparseArray<Order> convertFromJSONInOrder(JSONObject jSONObject){
		SimpleDateFormat sqlDateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

		if(jSONObject != null) {
			try {
				if(jSONObject.get("orders").equals(null))
					return null;
				JSONArray list = jSONObject.getJSONArray("orders");
				SparseArray<Order> orders = new SparseArray<Order>();
				for(int i = 0; i < list.length(); i++) {
					JSONArray object = (JSONArray) list.get(i);
					if(object != null) {
						int orderID = object.getInt(0);
						Order order = orders.get(orderID);
						if(order == null) {
							order = new Order();
							order.setId(orderID);
							order.setOrderText(object.getString(1));
							order.setStatus(object.getInt(4));
							order.setOrderType(object.getInt(6));
							order.setOrderTime(sqlDateFormat.parse(object.getString(7)));
							order.setTimeRequired(sqlDateFormat.parse(object.getString(12)));
							//order.setNumberOfPeople(!object.get(3).equals(null) ? object.getInt(11) :-1);
							order.setNumberOfPeople(object.getInt(11));
							order.setAddress(object.getString(13));
							orders.put(orderID, order);
						}
						//this means that order has a food
						if(!object.get(14).equals(null)) {
							DatabaseHelper helper = DatabaseHelper.getInstance(getApplicationContext());
							Food food = helper.getFoodById(object.getInt(16) + 10000);
							if(food == null) {
								food = new Food();
								/*
								 * add 10000 because in android database we always add 10000
								 */
								food.setId(object.getInt(16) + 10000);
							}
							food.setCount(object.getInt(17));
							order.addFood(food);
						}
					}
				}

				if(orders.size()>0)
					return orders;
			}catch(Exception e){
				e.printStackTrace();
			}
		}
		return null;
	}
}
