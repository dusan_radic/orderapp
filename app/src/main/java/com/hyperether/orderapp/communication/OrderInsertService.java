package com.hyperether.orderapp.communication;

import org.json.JSONException;
import org.json.JSONObject;

import android.app.Activity;
import android.app.IntentService;
import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Intent;
import android.support.v4.app.NotificationCompat;
import android.util.SparseArray;

import com.hyperether.orderapp.R;
import com.hyperether.orderapp.components.DataCache;
import com.hyperether.orderapp.components.DatabaseHelper;
import com.hyperether.orderapp.components.Order;
import com.hyperether.orderapp.user.Report;
import com.hyperether.orderapp.utils.NotificationSettings;
/**
 * This class is IntenService which send data to Web server
 * @author  Nebojsa Brankovic
 * 			Slobodan Prijic
 * @version 1.1, 2014-04-09
 *
 */
public class OrderInsertService extends IntentService {
	private int result = Activity.RESULT_CANCELED;
	public static final String RESULT = "result";
	public static final String NOTIFICATION = "com.hyperether.orderapp.communication.orderinsertservice";
	public static final int NOTIFICATION_ID = 6893;
	public static final String SEND_TYPE = "send_type";

	public OrderInsertService(String name) {
		super(name);
	}

	public OrderInsertService() {
		super("OrderInsertService");
	}

	@Override
	protected void onHandleIntent(Intent intent) {
		Order order = DataCache.getInstance().getOrder();
		int sendingType = intent.getIntExtra(SEND_TYPE, -1);
		switch (sendingType){
		case NotificationSettings.WEBSERVICE:
			SparseArray<JSONObject> serverOrder = ApiCalls.insertOrderItems(order,
					DataCache.getInstance().getImei(getApplicationContext()));

			if(isOderInserted(serverOrder)){
				order.setId(getOrderIDFromServer(serverOrder));
				result = Activity.RESULT_OK;
				notifyUser(order);
			}
			else {
				result = Activity.RESULT_CANCELED;
			}
			break;
		case NotificationSettings.EMAIL:
		case NotificationSettings.SMS:
			result = Activity.RESULT_OK;
			break;
		}

		if(result == Activity.RESULT_OK) {
			DatabaseHelper mDatabaseHelper = DatabaseHelper.getInstance(getApplicationContext());
			mDatabaseHelper.deleteDraftOrder();
			mDatabaseHelper.insertOrderSent(order);
			/*
			 * DataCache.getInstance().moveOrderInSentOrders();
			 * doesn't work because we need to get id from DB
			 */
			DataCache.getInstance().loadSentOrders(this);
			DataCache.getInstance().clearCurrentOrder();

			publishResults(result);

			/* NotificationService checks sentOrderList; this list has to be
			 * loaded before service is started
			 */
			if(sendingType == NotificationSettings.WEBSERVICE) {
				NotificationAlarm mNotificationAlarm = new NotificationAlarm();
				mNotificationAlarm.SetAlarm(getApplicationContext());
			}
		}
	}

	private void publishResults(int result) {
		Intent intent = new Intent(NOTIFICATION);
		intent.putExtra(RESULT, result);
		sendBroadcast(intent);
	}

	private void notifyUser(Order order) {
		NotificationManager mNotificationManager = (NotificationManager) getSystemService(NOTIFICATION_SERVICE);

		NotificationCompat.Builder builder = new NotificationCompat.Builder(
				this).setSmallIcon(R.drawable.ic_order_notify)
				.setContentTitle(getString(R.string.text_logo_name))
				.setContentText(getString(R.string.text_order_sent))
				.setDefaults(Notification.DEFAULT_SOUND);

		/*
		 * Clicking the notification itself displays Report
		 */
		Intent notificationIntent = new Intent(this, Report.class);
		notificationIntent.putExtra(Report.REPORT_TYPE, Report.REPORT_INSERTION);
		notificationIntent.putExtra(Report.REPORT_ORDER_ID, order.getId());
		PendingIntent pendingIntent = PendingIntent.getActivity(this, 0,
				notificationIntent, PendingIntent.FLAG_UPDATE_CURRENT);
		builder.setContentIntent(pendingIntent);

		mNotificationManager.notify(NOTIFICATION_ID, builder.build());
	}

	private int getOrderIDFromServer(SparseArray<JSONObject> serverOrder){
		JSONObject object = serverOrder.get(1);
		if(object != null){
			try {
				return object.getInt("id");
			} catch (JSONException e) {
				e.printStackTrace();
			}
		}
		return -1;
	}

	private boolean isOderInserted(SparseArray<JSONObject> serverOrder){
		JSONObject object = serverOrder.get(0);
		if(object != null){
			try {
				return object.getBoolean("responde");
			} catch (JSONException e) {
				e.printStackTrace();
			}
		}
		return false;
	}
}