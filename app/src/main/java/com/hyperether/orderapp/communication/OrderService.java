package com.hyperether.orderapp.communication;

import org.json.JSONException;
import org.json.JSONObject;

import android.app.Activity;
import android.app.IntentService;
import android.content.Intent;
import android.util.SparseArray;

import com.hyperether.orderapp.components.DataCache;
import com.hyperether.orderapp.components.DatabaseHelper;
import com.hyperether.orderapp.components.Promotion;

public class OrderService extends IntentService {
	private int result = Activity.RESULT_CANCELED;
	public static final String RESULT = "result";
	public static final String NOTIFICATION = "com.hyperether.orderapp.communication.orderservice";
	public static final String IMEI = "imei";

	public OrderService(String name) {
		super(name);
	}

	public OrderService() {
		super("OrderService");
	}

	@Override
	protected void onHandleIntent(Intent intent) {
		// Gets data from the incoming Intent
		String mImei = intent.getStringExtra(IMEI);
		SparseArray<JSONObject> apiResult = ApiCalls.getActiveMenuId(mImei);
		if(apiResult.size() > 0){
			DataCache dataCache = DataCache.getInstance();
			dataCache.setActiveMenuId(apiResult);
			/*
			 * Write promotion into DataCache and Database
			 */
			Promotion promotion = new Promotion();
			promotion.setPromotionList(apiResult, getApplicationContext());
			dataCache.setPromotion(promotion);
			//DatabaseHelper.getInstance(getApplicationContext()).insertFoodPromotionList(promotion.getPromotionList());
			DatabaseHelper.getInstance(getApplicationContext()).updateFoodPromotionList(promotion.getPromotionList());

			/*
			 * TODO; evaluacija: da li menadzer moze izgubti status menadzera?
			 */
			dataCache.setManager(isManager(apiResult));
			if(isManager(apiResult)){
				ManagerAlarm managerAlarm = new ManagerAlarm();
				managerAlarm.SetAlarm(getApplicationContext());
			}

			DataCache.getInstance().setDataLoaded(true);
			result = Activity.RESULT_OK;
		}
		publishResults(result);
	}

	private void publishResults(int result) {
		Intent intent = new Intent(NOTIFICATION);
		intent.putExtra(RESULT, result);
		sendBroadcast(intent);
	}

	private boolean isManager(SparseArray<JSONObject> result) {
		boolean mIsManager = false;
		JSONObject managerObject = result.get(2);
		if(managerObject!=null){
			try {
				mIsManager = managerObject.getBoolean("manager");
			} catch (JSONException e) {
				e.printStackTrace();
			}
		}
		return mIsManager;
	}
}
