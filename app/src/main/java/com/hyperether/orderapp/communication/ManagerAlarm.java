package com.hyperether.orderapp.communication;

import android.annotation.SuppressLint;
import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.os.PowerManager;
import android.util.Log;

import com.hyperether.orderapp.utils.Preferences;

public class ManagerAlarm extends BroadcastReceiver{
	private static int REFRESH_SECONDS = 30;
	@SuppressLint("Wakelock")
	@Override
	public void onReceive(Context context, Intent intent) {
		PowerManager pm = (PowerManager) context.getSystemService(Context.POWER_SERVICE);
		PowerManager.WakeLock wl = pm.newWakeLock(PowerManager.PARTIAL_WAKE_LOCK, "ManagerAlarm");
		wl.acquire();
		Log.i(Preferences.LOG_TAG, "Starting ManagerNotificationService");
		Intent mServiceIntent = new Intent(context, ManagerNotificationService.class);
		context.startService(mServiceIntent);
		wl.release();
	}

	public void SetAlarm(Context context) {
		Intent intent = new Intent(context, ManagerAlarm.class);
		boolean alarmUp = (PendingIntent.getBroadcast(context, 1, intent, PendingIntent.FLAG_NO_CREATE) != null);
		if(!alarmUp){
			AlarmManager am = (AlarmManager)context.getSystemService(Context.ALARM_SERVICE);
			PendingIntent pi = PendingIntent.getBroadcast(context, 1, intent, PendingIntent.FLAG_UPDATE_CURRENT);
			//TODO set real time after test
			am.setRepeating(AlarmManager.RTC_WAKEUP, System.currentTimeMillis(),1000 * REFRESH_SECONDS, pi);
		}

		Intent mServiceIntent = new Intent(context, ManagerNotificationService.class);
		context.startService(mServiceIntent);
	}

	public void CancelAlarm(Context context) {
		Intent intent = new Intent(context, ManagerAlarm.class);
		PendingIntent sender = PendingIntent.getBroadcast(context, 1, intent, PendingIntent.FLAG_UPDATE_CURRENT);
		AlarmManager alarmManager = (AlarmManager) context.getSystemService(Context.ALARM_SERVICE);
		alarmManager.cancel(sender);
	}
}