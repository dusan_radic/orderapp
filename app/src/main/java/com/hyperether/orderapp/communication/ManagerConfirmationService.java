package com.hyperether.orderapp.communication;

import android.app.IntentService;
import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.support.v4.app.NotificationCompat;
import android.support.v4.content.LocalBroadcastManager;
import android.util.Log;

import com.hyperether.orderapp.R;
import com.hyperether.orderapp.components.DataCache;
import com.hyperether.orderapp.components.DatabaseHelper;
import com.hyperether.orderapp.components.Order;
import com.hyperether.orderapp.manager.ManagerOrdersConfirmedList;
import com.hyperether.orderapp.utils.Preferences;

/**
 * This {@link IntentService} communicates with server
 * and sends confirmation data package
 * @authors Nebojsa Brankovic
 * 			Slobodan Prijic
 * @version 1.0, 2014-04-09
 */
public class ManagerConfirmationService extends IntentService {
	public static final String NOTIFICATION = "com.hyperether.orderapp.communication.ManagerConfirmationService";
	public static final int NOTIFICATION_ID = 6892;
	public static final String MORDER_ID = "morder_id";
	public static final String MRESPONDE = "responde";
	public static final String MTEXT = "text";

	public ManagerConfirmationService(String name) {
		super(name);
	}

	public ManagerConfirmationService() {
		super("ManagerConfirmationService");
	}

	@Override
	protected void onHandleIntent(Intent intent) {
		String responseText = "";
		responseText = intent.getStringExtra(MTEXT);
		int sOrderId = intent.getIntExtra(MORDER_ID, -1);
		int sConfirm = intent.getIntExtra(MRESPONDE, -1);

		if(ApiCalls.setConfirmOrder(sOrderId+"", sConfirm+"", responseText, DataCache.getInstance().getImei(getApplicationContext()))){
			Order order = DataCache.getInstance().getOrdersForConfirm().get(sOrderId);
			order.setStatus(sConfirm);
			order.setResponseText(responseText);
			// Update DB with confirmed order
			DatabaseHelper mDatabaseHelper = DatabaseHelper.getInstance(getApplicationContext());
			mDatabaseHelper.insertOrderConfirmed(order);
			// Order confirmed - remove it from the data map
			DataCache.getInstance().removeOrderFromConfirmMap(sOrderId + "");
			//show notification
			notifyManager();
			// Create intent to inform the ManagerOrdersPendingList activity that the data set has changed
			Intent msgIntent = new Intent("data-changed");
			LocalBroadcastManager.getInstance(this).sendBroadcast(msgIntent);
		} else {
			notificationCancel();
			Log.d(Preferences.LOG_TAG, "Server error");
		}
	}

	private void notifyManager() {
		NotificationManager mNotificationManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);

		NotificationCompat.Builder builder = new NotificationCompat.Builder(
				this).setSmallIcon(R.drawable.ic_order_notify)
				.setContentTitle(getString(R.string.text_logo_name))
				.setContentText(getString(R.string.text_confirmation_sent))
				.setAutoCancel(true)
				.setDefaults(Notification.DEFAULT_SOUND);

		/*
		 * Clicking the notification itself displays ManagerOrdersConfirmedList
		 */
		Intent notificationIntent = new Intent(this,
				ManagerOrdersConfirmedList.class);
		PendingIntent pendingIntent = PendingIntent.getActivity(this, 2,
				notificationIntent, PendingIntent.FLAG_CANCEL_CURRENT);
		builder.setContentIntent(pendingIntent);

		mNotificationManager.notify(NOTIFICATION_ID, builder.build());
	}

	private void notificationCancel() {
		NotificationManager mNotificationManager = (NotificationManager) this
				.getSystemService(Context.NOTIFICATION_SERVICE);
		mNotificationManager.cancel(NOTIFICATION_ID);
	}
}