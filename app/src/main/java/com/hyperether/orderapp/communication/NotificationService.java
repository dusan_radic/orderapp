package com.hyperether.orderapp.communication;

import java.util.Iterator;
import java.util.List;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.app.IntentService;
import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Intent;
import android.support.v4.app.NotificationCompat;
import android.util.SparseArray;

import com.hyperether.orderapp.R;
import com.hyperether.orderapp.components.DataCache;
import com.hyperether.orderapp.components.DatabaseHelper;
import com.hyperether.orderapp.components.Order;
import com.hyperether.orderapp.user.Report;

public class NotificationService extends IntentService {
	//public static final String RESULT = "result";
	//public static final String NOTIFICATION = "com.hyperether.orderapp.communication.notificationservice";
	public static final int NOTIFICATION_ID = 6895;
	private String responseText = null;

	public NotificationService(String name) {
		super(name);
	}

	public NotificationService() {
		super("NotificationService");
	}

	@Override
	protected void onHandleIntent(Intent intent) {
		int status = 1;
		int iOrderId;
		boolean keepAlarmRunning = false;

		List<Order> sentOrders = DataCache.getInstance().getOrdersSent(this);
		Iterator<Order> iterator = sentOrders.iterator();
		while(iterator.hasNext()) {
			Order mOrder = iterator.next();
			if(mOrder.getStatus() == Order.APP_PENDING) {
				iOrderId = mOrder.getId();
				String sOrderId = Integer.toString(iOrderId);
				SparseArray<JSONObject> apiResult = ApiCalls.getOrderNotification(sOrderId);
				/**
				 * status == 1			//Approved
				 * status == 0			//Cancelled
				 * status == -1			//Waiting
				 */
				status = getStatus(apiResult);
				if((status == Order.APP_APPROVED) || (status == Order.APP_DECLINED)){
					//DataCache.getInstance().updateSentOrderStatus(iOrderId, status);
					//if(responseText != null)
					//	DataCache.getInstance().setResponseText(responseText);
					mOrder.setResponseText(responseText);
					mOrder.setStatus(status);
					DatabaseHelper.getInstance(this).updateSentOrder(iOrderId, status, responseText);
					notifyUser(mOrder);
				}
				else if (status == Order.APP_PENDING){
					keepAlarmRunning = true;
				}
			}
		}

		if(!keepAlarmRunning){
			NotificationAlarm mNotificatioAlarm = new NotificationAlarm();
			mNotificatioAlarm.CancelAlarm(this);
		}

		//publishResults(status);
	}

	/*
	private void publishResults(int result) {
		Intent intent = new Intent(NOTIFICATION);
		intent.putExtra(RESULT, result);
		sendBroadcast(intent);
	}
	 */
	private void notifyUser(Order order) {
		NotificationManager mNotificationManager = (NotificationManager) getSystemService(NOTIFICATION_SERVICE);

		NotificationCompat.Builder builder = new NotificationCompat.Builder(
				this).setSmallIcon(R.drawable.ic_order_notify)
				.setContentTitle(getString(R.string.text_order_processed))
				.setContentText(order.getStatusText(getApplicationContext()))
				.setDefaults(Notification.DEFAULT_SOUND);

		/*
		 * Clicking the notification itself displays Report
		 */
		Intent notificationIntent = new Intent(this, Report.class);
		notificationIntent.putExtra(Report.REPORT_TYPE, Report.REPORT_NOTIFICATION);
		notificationIntent.putExtra(Report.REPORT_ORDER_ID, order.getId());
		PendingIntent pendingIntent = PendingIntent.getActivity(this, 1,
				notificationIntent, PendingIntent.FLAG_UPDATE_CURRENT);
		builder.setContentIntent(pendingIntent);

		mNotificationManager.notify(NOTIFICATION_ID, builder.build());
	}

	private int getStatus(SparseArray<JSONObject> result) {
		int status = -1;
		if(result != null){
			JSONObject statusObject = result.get(0);
			if(statusObject!=null){
				try {
					JSONArray list = statusObject.getJSONArray("status");
					/**
					 * This is number in JSONArray which we get from server.
					 * This are columns for table
					 * ..add...
					 */
					for (int i = 0 ; i<list.length() ; i++) {
						JSONArray  object = list.getJSONArray(i);
						status = (!object.get(0).equals(null) ? object.getInt(0) : -1);
						responseText = object.getString(1);
					}
				} catch (JSONException e) {
					e.printStackTrace();
				}
			}
		}
		return status;
	}
}