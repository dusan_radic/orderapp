package com.hyperether.orderapp.communication;

import java.util.ArrayList;
import java.util.List;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.util.Base64;
import android.util.SparseArray;

import com.hyperether.orderapp.components.Carte;
import com.hyperether.orderapp.components.DataCache;
import com.hyperether.orderapp.components.DatabaseHelper;
import com.hyperether.orderapp.components.Food;
import com.hyperether.orderapp.components.Item;

/**
 * JSONArray object
 * 
 * This is number in JSONArray which we get from server for menu.
 * This are columns for select
 * 0	-	menu.id
 * 1	-	menu.name
 * 2	-	menu.level
 * 3	-	menu.supId
 * 4	-	menu.hasSub
 * 5	-	menu.menuVersion
 * 6	-	menu.hasUpper
 * 7	-	menuitems.id
 * 8	-	menuitems.name
 * 9	-	menuitems.description
 * 10	-	menuitems.supMenuId
 * 11	-	menuitems.price
 * 12	-	menuitems.action
 * 13	-   menu.menuicon
 * 14	-   menuitems.icon
 */

public class MenuUpdate {
	Context mContext;
	List<Food> menuFoods = new ArrayList<Food>();
	List<Carte> menuCartes = new ArrayList<Carte>();
	DatabaseHelper mDatabaseHelper;

	public MenuUpdate(Context context){
		this.mContext = context;
		mDatabaseHelper = DatabaseHelper.getInstance(context);
	}

	public void updateMenu(SparseArray<JSONObject> result) {
		JSONObject actionObject = result.get(0);
		if(actionObject != null){
			try {
				JSONArray list = actionObject.getJSONArray("menuItems");
				for (int i = 0 ; i<list.length() ; i++) {
					//For JSONArray object see comment in file header
					JSONArray object = list.getJSONArray(i);
					//if not has id for Food that is it just Carte
					if(object.get(7).equals(null)){
						addCarte(object, i);
					}
					//has id for Food which means that we need to insert Food and to look if Carte exist or we need to insert and it
					else{
						//if Carte doesn't exist insert Carte
						if(!itemsExist(!object.get(0).equals(null) ? object.getInt(0) : -1)){
							addCarte(object, i);
						}
						//add Food
						addFood(object);
					}
				}
			} catch (JSONException e) {
				e.printStackTrace();
			}
			//recreate table orderMenu
			mDatabaseHelper.onUpgrade(mDatabaseHelper.getWritableDatabase(), 1,1);
			//insert Carte
			if(menuCartes!=null){
				for (Carte c : menuCartes){
					mDatabaseHelper.insertCarte(c);
				}
			}
			//insert Food
			if(menuFoods!=null){
				for (Food f: menuFoods){
					mDatabaseHelper.insertFoodMenu(f);
				}
			}

			//promotion is destroyed!
			mDatabaseHelper.insertFoodPromotionList(DataCache.getInstance().getPromotion().getPromotionList());
		}
	}

	//For JSONArray object see comment in file header
	private void addCarte(JSONArray object, int i){
		try {
			Carte carte = new Carte();
			carte.setId(!object.get(0).equals(null) ? object.getInt(0) : i);
			carte.setText(object.getString(1));
			carte.setSupermenuId(!object.get(3).equals(null) ? object.getInt(3) : 0);
			carte.setSupermenu(object.getInt(6));
			carte.setSubmenu(object.getInt(4));

			byte[] decodedString = Base64.decode(object.getString(13).getBytes(), Base64.DEFAULT);
			Bitmap icon = BitmapFactory.decodeByteArray(decodedString, 0, decodedString.length);

			carte.setImage(icon);
			menuCartes.add(carte);
		} catch (JSONException e) {
			e.printStackTrace();
		}
	}

	//For JSONArray object see comment in file header
	private void addFood(JSONArray object){
		try {
			Food food = new Food();
			food.setId(!object.get(7).equals(null) ? 10000+object.getInt(7) : -1);
			food.setText(object.getString(8));
			food.setDescription(object.getString(9));
			food.setPrice(!object.get(11).equals(null) ? object.getDouble(11) : 0.0);
			food.setSupermenuId(!object.get(10).equals(null) ? object.getInt(10) : -1);

			byte[] decodedString = Base64.decode(object.getString(14).getBytes(), Base64.DEFAULT);
			Bitmap icon = BitmapFactory.decodeByteArray(decodedString, 0, decodedString.length);

			food.setImage(icon);

			menuFoods.add(food);
		} catch (JSONException e) {
			e.printStackTrace();
		}
	}
	/**
	 * Check if Carte id exist in menuCartes
	 * @param id of Carte
	 * @return true if Carte with id exist in list
	 */
	private boolean itemsExist(int id){
		if(menuCartes!=null && !menuCartes.isEmpty()){
			for(Item p : menuCartes){
				if(p.getId()==id){
					return true;
				}
			}
		}
		return false;
	}
}
