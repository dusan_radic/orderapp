package com.hyperether.orderapp.communication;

import java.io.IOException;
import java.nio.charset.Charset;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.List;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.mime.MultipartEntity;
import org.apache.http.entity.mime.content.StringBody;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.util.EntityUtils;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.annotation.SuppressLint;

import com.hyperether.orderapp.components.Food;
import com.hyperether.orderapp.components.Order;
/**
 * This clas make URL from string URL and add params. 
 * @author Nebojsa Brankovic
 * @version 1.0, 2014-01-31
 *
 */

public class Communicator {
	@SuppressLint("SimpleDateFormat")
	SimpleDateFormat sqlDateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
	/**
	 * In even number of params it is parameter and in odd number of params it is value od parameter.
	 * If length of params == 1, we just add that param to URL
	 * @param sUrl start string for url
	 * @param params name of parameter and value of parameter
	 * @return JSONArray responde from web server
	 */

	public JSONArray returnJSONArrayFromURL(String sUrl,
			String... params) {
		JSONArray jsonArray = null;

		StringBuilder sbUrl = new StringBuilder(sUrl);
		if (params != null && params.length > 0){
			if((params.length % 2) ==0){
				for (int i = 0; i < params.length; i++) {
					if(i==0){
						sbUrl.append("?"+params[i].toString()+"=");
						sbUrl.append(params[i+1].toString());
						i++;
					}
					else{
						sbUrl.append("&"+params[i].toString()+"=");
						sbUrl.append(params[i+1].toString());
						i++;
					}
				}
			}
			else{
				sbUrl.append(params[0].toString());
			}
		}


		HttpGet httpGet = new HttpGet(sbUrl.toString());
		try {
			DefaultHttpClient httpClient = new DefaultHttpClient();
			HttpResponse httpResponse = httpClient.execute(httpGet);
			int status = httpResponse.getStatusLine().getStatusCode();
			if (status==200){
				HttpEntity httpEntity = httpResponse.getEntity();				
				String sData = EntityUtils.toString(httpEntity);
				JSONObject fieldsJson = new JSONObject(sData);
				jsonArray = fieldsJson.getJSONArray("data");
				httpEntity.consumeContent();

			}
		} catch (ClientProtocolException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		} catch (JSONException e) {
			e.printStackTrace();
		}
		return jsonArray;
	}


	/**
	 * @param sUrl start string for url
	 * @param params name of parameter and value of parameter
	 * @return JSONArray responde from web server
	 */

	public JSONArray returnJSONArrayFromURLPost(String sUrl, Order order, String sImei,
			String... params) {
		JSONArray jsonArray = null;

		StringBuilder sbUrl = new StringBuilder(sUrl);
		HttpPost httpPost = new HttpPost(sbUrl.toString());
		try {
			DefaultHttpClient httpClient = new DefaultHttpClient();
			MultipartEntity reqEntity = new MultipartEntity();

			StringBody foodList = new StringBody(convertOrderToJSON(order.getFoodList()).toString(),Charset.forName("UTF8"));
			reqEntity.addPart("listFood", foodList);
			StringBody imei = new StringBody(sImei);
			reqEntity.addPart("imei",imei);
			StringBody orderTime = new StringBody(sqlDateFormat.format(order.getOrderTime()));
			reqEntity.addPart("orderTime",orderTime);
			StringBody orderType = new StringBody(order.getOrderType()+"");
			reqEntity.addPart("orderType",orderType);
			StringBody timeZone = new StringBody(Calendar.getInstance().getTimeZone().getRawOffset()+"");
			reqEntity.addPart("timeZone",timeZone);
			StringBody statusOrder = new StringBody(order.getStatus()+"");
			reqEntity.addPart("statusOrder",statusOrder);
			StringBody dateTime = new StringBody(sqlDateFormat.format(order.getTimeRequired()));
			reqEntity.addPart("dateTime",dateTime);
			StringBody address = new StringBody(order.getAddress()+"");
			reqEntity.addPart("address",address);
			StringBody numberOfPeople = new StringBody(order.getNumberOfPeople()+"");
			reqEntity.addPart("numberOfPeople",numberOfPeople);
			StringBody orderText = new StringBody(order.getOrderText()+"");
			reqEntity.addPart("orderText",orderText);

			httpPost.setEntity(reqEntity);



			HttpResponse httpResponse = httpClient.execute(httpPost);
			int status = httpResponse.getStatusLine().getStatusCode();
			if (status==200){
				HttpEntity httpEntity = httpResponse.getEntity();				
				String sData = EntityUtils.toString(httpEntity);
				JSONObject fieldsJson = new JSONObject(sData);
				jsonArray = fieldsJson.getJSONArray("data");
				httpEntity.consumeContent();
			}
		} catch (ClientProtocolException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		} catch (JSONException e) {
			e.printStackTrace();
		}
		return jsonArray;
	}


	private JSONArray convertOrderToJSON(List<Food> order) throws JSONException{
		JSONArray orderItems = new JSONArray();
		for(Food f :order){
			JSONObject  object = new JSONObject();
			object.accumulate("itemid",f.getId()-10000);
			object.accumulate("count", f.getCount());
			orderItems.put(object);
		}
		return orderItems;

	}
}