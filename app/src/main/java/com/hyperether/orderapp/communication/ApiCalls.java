package com.hyperether.orderapp.communication;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.util.SparseArray;

import com.hyperether.orderapp.components.Order;
/**
 * 
 * @author Nebojsa Brankovic
 * @version 1.x, 2014-02-18 - add method insertOrderItems
 *
 */

public class ApiCalls {

	private static String serverUrl = "http://hyperether.com/Mobile/orderappserver";
	//private static String serverUrl = "http://192.168.3.4:80/OrderAppServer";
	//private static String serverUrl = "http://192.168.137.1:8080/quercus-4.0.37/app";
	private static String sActiveMenuURL = serverUrl+"/OrderAppMenuVersion.php?imei=";

	private static String sMenuItemsURL = serverUrl+"/OrderAppMenuItems.php";
	private static String sOrderInsertUrl = serverUrl+"/OrderAppSaveOrderReservation.php";
	private static String sOrderNotificationURL = serverUrl+"/OrderAppUserNotification.php?orderId=";

	private static String sOrderConfirmUrl = serverUrl+"/OrderAppConfirmOrder.php";
	private static String sOrderForManagerUrl = serverUrl+"/OrderAppOrderForManager.php";

	public static SparseArray<JSONObject> getActiveMenuId(String imei){
		SparseArray<JSONObject> returnValue = new SparseArray<JSONObject>();
		JSONArray jsonArray = new Communicator().returnJSONArrayFromURL(sActiveMenuURL, imei);
		try {
			if(jsonArray!=null){
				JSONObject jsonObject = (JSONObject) jsonArray.get(0);
				returnValue.put(0, jsonObject);
				JSONObject jsonObject1 = (JSONObject) jsonArray.get(1);
				returnValue.put(1, jsonObject1);
				JSONObject jsonObject2 = (JSONObject) jsonArray.get(2);
				returnValue.put(2, jsonObject2);
			}
		} catch (JSONException e) {
			e.printStackTrace();
		}
		return returnValue;
	}

	public static SparseArray<JSONObject> getMenuItems(String menuVersion){
		SparseArray<JSONObject> returnValue = new SparseArray<JSONObject>();
		JSONArray jsonArray = new Communicator().returnJSONArrayFromURL(sMenuItemsURL,"menuId",menuVersion);
		try {
			if(jsonArray!=null){
				JSONObject jsonObject = (JSONObject) jsonArray.get(0);
				returnValue.put(0, jsonObject);
				JSONObject jsonObject1 = (JSONObject) jsonArray.get(1);
				returnValue.put(1, jsonObject1);
			}
		} catch (JSONException e) {
			e.printStackTrace();
		}
		return returnValue;
	}

	public static SparseArray<JSONObject> getOrderNotification(String sOrderId){
		SparseArray<JSONObject> returnValue = new SparseArray<JSONObject>();
		JSONArray jsonArray = new Communicator().returnJSONArrayFromURL(sOrderNotificationURL, sOrderId);
		try {
			if(jsonArray!=null){
				JSONObject jsonObject = (JSONObject) jsonArray.get(0);
				returnValue.put(0, jsonObject);
			}
		} catch (JSONException e) {
			e.printStackTrace();
		}
		return returnValue;
	}

	public static SparseArray<JSONObject> insertOrderItems(Order order, String imei){
		SparseArray<JSONObject> returnValue = new SparseArray<JSONObject>();
		JSONArray jsonArray = new Communicator().returnJSONArrayFromURLPost(sOrderInsertUrl, order, imei);
		try {
			if(jsonArray!=null){
				JSONObject jsonObject = (JSONObject) jsonArray.get(0);
				returnValue.put(0, jsonObject);
				JSONObject jsonObject1 = (JSONObject) jsonArray.get(1);
				returnValue.put(1, jsonObject1);
			}
		} catch (JSONException e) {
			e.printStackTrace();
		}
		return returnValue;
	}

	public static boolean setConfirmOrder(String sOrderId, String sConfirm,
			String responseText, String imei) {
		String responseTextEnc = "";
		try {
			responseTextEnc = URLEncoder.encode(responseText, "UTF-8");
		} catch (UnsupportedEncodingException e1) {
			e1.printStackTrace();
		}

		JSONArray jsonArray = new Communicator().returnJSONArrayFromURL(
				sOrderConfirmUrl, "orderId", sOrderId, "confirm", sConfirm,
				"responseText", responseTextEnc, "imei", imei);
		try {
			if (jsonArray != null) {
				JSONObject jsonObject = (JSONObject) jsonArray.get(0);
				return jsonObject.getBoolean("responde");
			}
		} catch (JSONException e) {
			e.printStackTrace();
		}
		return false;
	}

	public static JSONObject getOrderForManager(String sImei){
		JSONArray jsonArray = new Communicator().returnJSONArrayFromURL(sOrderForManagerUrl,"imei",sImei);
		try {
			if(jsonArray!=null){
				JSONObject jsonObject =  (JSONObject) jsonArray.get(0);
				if(jsonObject!=null ){
					return jsonObject;
				}
			}
		} catch (JSONException e) {
			e.printStackTrace();
		}
		return null;
	}
}
