package com.hyperether.orderapp;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.Toast;

import com.hyperether.orderapp.communication.ManagerAlarm;
import com.hyperether.orderapp.communication.MenuUpdateService;
import com.hyperether.orderapp.communication.NotificationAlarm;
import com.hyperether.orderapp.communication.OrderInsertService;
import com.hyperether.orderapp.communication.OrderService;
import com.hyperether.orderapp.components.DataCache;
import com.hyperether.orderapp.components.Order;
import com.hyperether.orderapp.listeners.MainNavigationOnClickListener;
import com.hyperether.orderapp.manager.ManagerOrdersConfirmedList;
import com.hyperether.orderapp.manager.ManagerOrdersPendingList;
import com.hyperether.orderapp.user.Info;
import com.hyperether.orderapp.user.OldOrdersList;
import com.hyperether.orderapp.user.OrderList;
import com.hyperether.orderapp.utils.NotificationSettings;
import com.hyperether.orderapp.utils.Preferences;


/**
 * Main activity class - OrderApp
 * 
 * @version January, 2014
 *
 */
public class OrderMain extends Activity implements OnClickListener {
	private static final int ORDER_NAVIGATION = 1;
	private int mLastMenuId = -1;
	ProgressDialog mDialog;
	/*
	 * This variable handles case:
	 * 1. server is not accessible
	 * 2. screen is rotated
	 * We do not want to try connecting every time
	 */
	private boolean isDataLoaded = false;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_order_main);
		initLayout();
	}

	@Override
	protected void onResume() {
		super.onResume();
		registerReceiver(receiverUpdatePromotion, new IntentFilter(OrderService.NOTIFICATION));
		registerReceiver(receiverUpdateMenu, new IntentFilter(MenuUpdateService.NOTIFICATION));
		registerReceiver(receiverOrderInsert, new IntentFilter(OrderInsertService.NOTIFICATION));

		if(!DataCache.getInstance().isDataLoaded() && !this.isDataLoaded){
			DataCache.getInstance().init(this);

			mDialog = new ProgressDialog(this);
			mDialog.setMessage(this.getString(R.string.text_promotion_loading));
			mDialog.setIndeterminate(true);
			mDialog.setCancelable(true);
			mDialog.show();

			Intent mServiceIntent = new Intent(this, OrderService.class);
			mServiceIntent.putExtra(OrderService.IMEI, DataCache.getInstance().getImei(this));
			startService(mServiceIntent);
		}
		this.isDataLoaded = true;
	}

	@Override
	protected void onPause() {
		super.onPause();
		unregisterReceiver(receiverUpdatePromotion);
		unregisterReceiver(receiverUpdateMenu);
		unregisterReceiver(receiverOrderInsert);
	}

	private final BroadcastReceiver receiverUpdatePromotion = new BroadcastReceiver() {
		@Override
		public void onReceive(Context context, Intent intent) {
			mDialog.dismiss();
			Bundle bundle = intent.getExtras();
			if (bundle != null) {
				int resultCode = bundle.getInt(OrderService.RESULT);
				if (resultCode == RESULT_OK) {
					Toast.makeText(OrderMain.this, context.getString(R.string.text_download_complete), Toast.LENGTH_LONG).show();
					int menuId = DataCache.getInstance().getActiveMenuId();
					SharedPreferences settings = getSharedPreferences(Preferences.PREF_FILE, 0);

					//ako je menadzer prvi put ukljucio aplikaciju, treba azurirati options menu
					if(DataCache.getInstance().getManager()){
						ActivityCompat.invalidateOptionsMenu(OrderMain.this);
					}
					/*
					 * Update manager status:
					 * important transitions: false -> true, true -> false
					 */
					SharedPreferences.Editor editor = settings.edit();
					editor.putBoolean(Preferences.PREF_IS_MANAGER, DataCache.getInstance().getManager());
					editor.commit();

					mLastMenuId = settings.getInt(Preferences.PREF_LAST_MENU_ID, -1);
					if(menuId != mLastMenuId){
						mDialog.setMessage(context.getString(R.string.text_updating_menu));
						mDialog.show();
						Intent mServiceIntent = new Intent(OrderMain.this, MenuUpdateService.class);
						mServiceIntent.putExtra(MenuUpdateService.MENU_ID, menuId+"");
						startService(mServiceIntent);
					}
				} else {
					Toast.makeText(OrderMain.this, context.getString(R.string.text_download_failed), Toast.LENGTH_LONG).show();
				}
			}
		}
	};

	private final BroadcastReceiver receiverUpdateMenu = new BroadcastReceiver() {
		@Override
		public void onReceive(Context context, Intent intent) {
			mDialog.dismiss();
			Bundle bundle = intent.getExtras();
			if (bundle != null) {
				int resultCode = bundle.getInt(MenuUpdateService.RESULT);
				if (resultCode == RESULT_OK) {
					Toast.makeText(OrderMain.this, context.getString(R.string.text_download_complete), Toast.LENGTH_LONG).show();

					int menuId = DataCache.getInstance().getActiveMenuId();
					SharedPreferences settings = getSharedPreferences(Preferences.PREF_FILE, 0);
					SharedPreferences.Editor editor = settings.edit();
					editor.putInt(Preferences.PREF_LAST_MENU_ID, menuId);
					editor.commit();
				} else {
					Toast.makeText(OrderMain.this, context.getString(R.string.text_download_failed), Toast.LENGTH_LONG).show();
				}
			}
		}
	};

	private final BroadcastReceiver receiverOrderInsert = new BroadcastReceiver() {
		@Override
		public void onReceive(Context context, Intent intent) {
			Bundle bundle = intent.getExtras();
			if (bundle != null) {
				String report = null;
				int resultCode = bundle.getInt(OrderInsertService.RESULT);
				if (resultCode == RESULT_OK) {
					report = context.getString(R.string.text_order_sending_ok);
				} else {
					report = context.getString(R.string.text_order_sending_failed);
				}

				AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(context);
				alertDialogBuilder
				.setMessage(String.valueOf(report))
				.setCancelable(false)
				.setPositiveButton("Ok",new DialogInterface.OnClickListener() {
					@Override
					public void onClick(DialogInterface dialog,int id) {

					}
				});
				AlertDialog alertDialog = alertDialogBuilder.create();
				alertDialog.show();
			}
		}
	};

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		super.onCreateOptionsMenu(menu);
		return true;
	}

	@Override
	public boolean onPrepareOptionsMenu(Menu menu) {
		menu.clear();
		MenuInflater inflater = getMenuInflater();
		if(DataCache.getInstance().getManager())
			inflater.inflate(R.menu.manager_main, menu);
		else
			inflater.inflate(R.menu.user_main, menu);
		return super.onPrepareOptionsMenu(menu);
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		Intent intent;

		switch (item.getItemId()) {
		case R.id.action_settings:
			intent = new Intent(this, NotificationSettings.class);
			startActivity(intent);
			return true;
		case R.id.action_stop_services:
			NotificationAlarm mNotificatioAlarm = new NotificationAlarm();
			mNotificatioAlarm.CancelAlarm(this);
			ManagerAlarm mManagerAlarm = new ManagerAlarm();
			mManagerAlarm.CancelAlarm(getApplicationContext());
			return true;
		case R.id.action_old_orders:
			intent = new Intent(this, OldOrdersList.class);
			startActivity(intent);
			return true;
		case R.id.action_pending_orders:
			intent = new Intent(this, ManagerOrdersPendingList.class);
			startActivity(intent);
			return true;
		case R.id.action_confirmed_orders:
			intent = new Intent(this, ManagerOrdersConfirmedList.class);
			startActivity(intent);
			return true;
		case R.id.action_check_order:
			SharedPreferences settings = getSharedPreferences(Preferences.PREF_FILE, 0);
			int sendingType = settings.getInt("notificationMethod", -1);
			if(sendingType == NotificationSettings.WEBSERVICE) {
				NotificationAlarm mNotificationAlarm = new NotificationAlarm();
				mNotificationAlarm.SetAlarm(getApplicationContext());
			}
			return true;
		default:
			return super.onOptionsItemSelected(item);
		}
	}

	/**
	 * Method that initializes the main menu button layout
	 * 
	 */
	private void initLayout() {
		/*
		 * Capture our buttons from layout
		 * Register the onClick listener with the implementation above
		 */
		Button mButtonPromotion, mButtonMenu, mButtonReservation, mButtonOrder;
		ImageView mAdvertisement;
		MainNavigationOnClickListener listener = new MainNavigationOnClickListener(this, this);

		mButtonPromotion = (Button) findViewById(R.id.buttonPromotion);
		mButtonPromotion.setOnClickListener(listener.promotionListener);

		mButtonMenu = (Button) findViewById(R.id.buttonMenu);
		mButtonMenu.setOnClickListener(listener.orderMenuListener);

		mButtonReservation = (Button) findViewById(R.id.buttonReservation);
		mButtonReservation.setOnClickListener(listener.reservationListener);

		mButtonOrder = (Button) findViewById(R.id.buttonOrderOverview);
		mButtonOrder.setOnClickListener(this);

		mAdvertisement = (ImageView) findViewById(R.id.imageViewAd);
		mAdvertisement.setOnClickListener(listener.onAdListener);
	}

	@Override
	public void onClick(View v) {
		switch(v.getId()){
		case R.id.buttonOrderOverview:
			Order mOrder = DataCache.getInstance().getOrder();
			if(!mOrder.getFoodList().isEmpty()){
				Intent intent = new Intent(this, OrderList.class);
				startActivityForResult(intent, ORDER_NAVIGATION);
			} else {
				Toast.makeText(this, this.getString(R.string.text_order_empty), Toast.LENGTH_LONG).show();
			}
			break;
		case R.id.imageViewLogo:
		case R.id.textViewEnterRes:
			Intent intent = new Intent(this, Info.class);
			startActivity(intent);
			break;
		}
	}

	@Override
	protected void onDestroy() {
		super.onDestroy();
		DataCache.getInstance().setDataLoaded(false);
	}

	@Override
	public void onSaveInstanceState(Bundle savedInstanceState) {
		savedInstanceState.putBoolean("isDataLoaded", isDataLoaded);
		super.onSaveInstanceState(savedInstanceState);
	}

	@Override
	public void onRestoreInstanceState(Bundle savedInstanceState) {
		super.onRestoreInstanceState(savedInstanceState);
		isDataLoaded = savedInstanceState.getBoolean("isDataLoaded");
	}
}
