package com.hyperether.orderapp.manager;

import java.util.List;

import android.app.ListActivity;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.support.v4.content.LocalBroadcastManager;
import android.view.View;
import android.widget.ListView;

import com.hyperether.orderapp.R;
import com.hyperether.orderapp.adapters.OrderListManagerBaseAdapter;
import com.hyperether.orderapp.components.DataCache;
import com.hyperether.orderapp.components.Order;

/**
 * Class that presents the orders waiting for manager response
 * 
 * @version March, 2014
 *
 */
public class ManagerOrdersPendingList extends ListActivity {

	private OrderListManagerBaseAdapter adapter;
	List<Order> mOrders = null;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_element_list);

		loadData();
		createList();
	}

	private void loadData(){
		mOrders = DataCache.getInstance().getOrdersPending();
	}

	private void createList(){
		ListView list = (ListView) findViewById(android.R.id.list);
		adapter = new OrderListManagerBaseAdapter(this, mOrders);
		if(adapter.getCount() > 0) {
			list.setAdapter(adapter);
		}
	}

	@Override
	protected void onListItemClick(ListView l, View v, int position, long id) {
		OrderListManagerBaseAdapter.ViewHolder tmpHolder = (OrderListManagerBaseAdapter.ViewHolder) v.getTag();

		// Passing the unique database id of the chosen route in order to draw it correctly
		Intent intent = new Intent(this, ManagerOrder.class);
		intent.putExtra("MANAGER_ORDER_ID", tmpHolder.orderId);
		intent.putExtra("MANAGER_ORDER_POS", position);
		intent.putExtra("type", ManagerOrder.PENDING);
		startActivity(intent);
	}

	// handler for received Intents for the "data-changed" event
	private final BroadcastReceiver mMessageReceiver = new BroadcastReceiver() {
		@Override
		public void onReceive(Context context, Intent intent) {
			// Refresh the list data and update adapter
			mOrders.clear();
			mOrders = DataCache.getInstance().getOrdersPending();
			adapter.updateMenu(mOrders);
			if(mOrders.isEmpty()){
				finish();
			}
		}
	};

	@Override
	protected void onResume() {
		super.onResume();
		// Register mMessageReceiver to receive messages.
		LocalBroadcastManager.getInstance(this).registerReceiver(mMessageReceiver,
				new IntentFilter("data-changed"));
	}

	@Override
	protected void onPause() {
		// Unregister since the activity is not visible
		LocalBroadcastManager.getInstance(this).unregisterReceiver(mMessageReceiver);
		super.onPause();
	}
}
