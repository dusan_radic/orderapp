package com.hyperether.orderapp.manager;

import java.util.ArrayList;
import java.util.List;

import android.app.Activity;
import android.app.ListActivity;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemLongClickListener;
import android.widget.ListView;
import android.widget.Toast;

import com.hyperether.orderapp.R;
import com.hyperether.orderapp.adapters.OrderListManagerBaseAdapter;
import com.hyperether.orderapp.components.DataCache;
import com.hyperether.orderapp.components.Order;
import com.hyperether.orderapp.listeners.SwipeDismissListViewTouchListener;

/**
 * Class that presents the orders manager already responded
 * 
 * @version March, 2014
 *
 */
public class ManagerOrdersConfirmedList extends ListActivity{
	private static final int ORDER_NAVIGATION = 1;
	private OrderListManagerBaseAdapter adapter;
	List<Order> mOrders = new ArrayList<Order>();

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_element_list);

		loadData();
		createList();
		setListActions();
	}

	private void loadData(){
		mOrders = DataCache.getInstance().getOrdersConfirmed();
	}

	private void createList(){
		ListView list = (ListView) findViewById(android.R.id.list);
		adapter = new OrderListManagerBaseAdapter(this, mOrders);
		if(adapter.getCount() > 0) {
			list.setAdapter(adapter);
		}
	}

	@Override
	protected void onListItemClick(ListView l, View v, int position, long id) {
		OrderListManagerBaseAdapter.ViewHolder tmpHolder = (OrderListManagerBaseAdapter.ViewHolder) v.getTag();

		// Passing the unique database id of the chosen route in order to draw it correctly
		Intent intent = new Intent(this, ManagerOrder.class);
		intent.putExtra("MANAGER_ORDER_ID", tmpHolder.orderId);
		intent.putExtra("MANAGER_ORDER_POS", position);
		intent.putExtra("type", ManagerOrder.CONFIRMED);
		startActivityForResult(intent, ORDER_NAVIGATION);
	}

	@Override
	public void onActivityResult(int requestCode, int resultCode, Intent data) {
		super.onActivityResult(requestCode, resultCode, data);
		switch(requestCode) {
		case (ORDER_NAVIGATION) :
			if (resultCode == Activity.RESULT_OK) {
				Intent resultIntent = new Intent();
				setResult(Activity.RESULT_OK, resultIntent);
				finish();
			} else if (resultCode == Activity.RESULT_FIRST_USER) {
				if(data.hasExtra("MANAGER_ORDER_POS")){
					int pos = data.getExtras().getInt("MANAGER_ORDER_POS");
					if(pos >= 0){
						adapter.remove(adapter.getItem(pos));
						adapter.notifyDataSetChanged();
					}
					DataCache.getInstance().loadConfirmedOrders(this);
					mOrders = DataCache.getInstance().getOrdersConfirmed();
					if(mOrders.isEmpty()){
						finish();
					}
				}
			}
		break;
		}
	}

	private void setListActions(){
		getListView().setOnItemLongClickListener(new OnItemLongClickListener() {
			@Override
			public boolean onItemLongClick(AdapterView<?> parent, View view,
					int position, long id) {
				OrderListManagerBaseAdapter.ViewHolder tmpHolder = (OrderListManagerBaseAdapter.ViewHolder) view.getTag();
				if(tmpHolder!=null && tmpHolder.orderId>0){
					Toast.makeText(getApplicationContext(), getApplicationContext().getString(R.string.text_order_id) +" = " + tmpHolder.orderId, Toast.LENGTH_LONG).show();
				}
				else{
					Toast.makeText(getApplicationContext(), getApplicationContext().getString(R.string.text_no_order), Toast.LENGTH_LONG).show();
				}
				return true;
			}
		});

		if(Build.VERSION.SDK_INT >= 12){
			ListView listView = getListView();
			// Create a ListView-specific touch listener. ListViews are given special treatment because
			// by default they handle touches for their list items... i.e. they're in charge of drawing
			// the pressed state (the list selector), handling list item clicks, etc.
			SwipeDismissListViewTouchListener touchListener = new SwipeDismissListViewTouchListener(
					listView,
					new SwipeDismissListViewTouchListener.DismissCallbacks() {
						@Override
						public boolean canDismiss(int position) {
							return true;
						}

						@Override
						public void onDismiss(ListView listView, int[] reverseSortedPositions) {
							for (int position : reverseSortedPositions) {
								adapter.remove(adapter.getItem(position));
							}
							adapter.notifyDataSetChanged();
						}
					});
			listView.setOnTouchListener(touchListener);
			// Setting this scroll listener is required to ensure that during ListView scrolling,
			// we don't look for swipes.
			listView.setOnScrollListener(touchListener.makeScrollListener());
		}
	}
}
