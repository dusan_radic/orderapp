package com.hyperether.orderapp.manager;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.hyperether.orderapp.R;
import com.hyperether.orderapp.adapters.FoodListOverviewBaseAdapter;
import com.hyperether.orderapp.communication.ManagerConfirmationService;
import com.hyperether.orderapp.components.DataCache;
import com.hyperether.orderapp.components.Order;

public class ManagerOrder extends Activity implements OnClickListener{
	public static int PENDING = 0;
	public static int CONFIRMED = 1;
	private FoodListOverviewBaseAdapter adapter;
	Button buttonConfirm;
	Button buttonConfirmCancel;
	Button buttonConfirmNo;
	EditText editText;

	private int orderId;
	private int positionInParent;
	private Order mOrder;
	private int activityType;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_manager_order);

		Bundle b = getIntent().getExtras();
		orderId = b.getInt("MANAGER_ORDER_ID");
		positionInParent = b.getInt("MANAGER_ORDER_POS");
		activityType = b.getInt("type", -1);

		if(activityType == PENDING)
			mOrder = DataCache.getInstance().getOrdersForConfirm().get(orderId);
		else if (activityType == CONFIRMED)
			mOrder = DataCache.getInstance().getOrderConfirmed(orderId);

		initLayout();
		createList();
	}

	private void initLayout(){
		Button buttonConfirm = (Button) findViewById(R.id.button_confirm_confirm);
		Button buttonConfirmCancel = (Button) findViewById(R.id.button_confirm_cancel);
		Button buttonConfirmNo = (Button) findViewById(R.id.button_confirm_no);
		buttonConfirmCancel.setOnClickListener(this);
		buttonConfirmNo.setOnClickListener(this);
		editText = (EditText) findViewById(R.id.editText_responde);

		if(activityType == PENDING){
			buttonConfirm.setOnClickListener(this);
		} else if (activityType == CONFIRMED){
			buttonConfirm.setVisibility(View.GONE);
			RelativeLayout.LayoutParams params = (RelativeLayout.LayoutParams)buttonConfirmCancel.getLayoutParams();
			params.addRule(RelativeLayout.ALIGN_PARENT_LEFT);
			buttonConfirmCancel.setLayoutParams(params); //causes layout update
			buttonConfirmNo.setText(this.getString(R.string.button_delete));
			editText.setClickable(false);
			editText.setFocusable(false);
			editText.setText(mOrder.getResponseText());
		}

		TextView textPrice = (TextView) findViewById(R.id.textCurrentTotalPrice);
		textPrice.setText(mOrder.getTotalPriceString(this));

		TextView textOrderType = (TextView) findViewById(R.id.textOrderType);
		switch (mOrder.getOrderType()) {
		case Order.ORDER_FOOD_DELIVERY:
			textOrderType.setText("D:" + mOrder.getAddress());
			break;
		case Order.ORDER_FOOD_ONSITE:
			textOrderType.setText(this.getString(R.string.text_persons) + ":" + mOrder.getNumberOfPeople());
			break;
		case Order.ORDER_FOOD_PICKUP:
			textOrderType.setText(R.string.text_pickup);
			break;
		case Order.ORDER_RESERVATION:
			textOrderType.setText(this.getString(R.string.text_reservation_persons) + " " + mOrder.getNumberOfPeople());
			textPrice.setVisibility(View.GONE);
			TextView textTotal = (TextView) findViewById(R.id.textTotal);
			textTotal.setVisibility(View.GONE);
			break;
		default:
			textOrderType.setText("");
		}
	}

	private void createList(){
		ListView list = (ListView) findViewById(android.R.id.list);
		adapter = new FoodListOverviewBaseAdapter(this, mOrder.getFoodList());
		if(adapter.getCount() > 0) {
			list.setAdapter(adapter);
		}
	}

	@Override
	public void onClick(View v) {
		switch(v.getId()){
		case R.id.button_confirm_confirm:
			if(mOrder!=null){
				Intent intent = new Intent(this, ManagerConfirmationService.class);
				intent.putExtra(ManagerConfirmationService.MORDER_ID, mOrder.getId());
				intent.putExtra(ManagerConfirmationService.MRESPONDE, Order.APP_APPROVED);
				intent.putExtra(ManagerConfirmationService.MTEXT, editText.getText().toString().trim());
				startService(intent);
			}
			finish();
			break;
		case R.id.button_confirm_cancel:
			finish();
			break;
		case R.id.button_confirm_no:
			if(activityType == PENDING) {
				if(mOrder!=null){
					Intent intent = new Intent(this, ManagerConfirmationService.class);
					intent.putExtra(ManagerConfirmationService.MORDER_ID, mOrder.getId());
					intent.putExtra(ManagerConfirmationService.MRESPONDE, Order.APP_DECLINED);
					intent.putExtra(ManagerConfirmationService.MTEXT, editText.getText().toString().trim());
					startService(intent);
				}
				finish();
			} else if(activityType == CONFIRMED) {
				Intent resultIntent = new Intent();
				resultIntent.putExtra("MANAGER_ORDER_POS", positionInParent);
				resultIntent.putExtra("MANAGER_ORDER_ID", orderId);
				setResult(Activity.RESULT_FIRST_USER, resultIntent);
				finish();
			}
			break;
		}
	}
}
