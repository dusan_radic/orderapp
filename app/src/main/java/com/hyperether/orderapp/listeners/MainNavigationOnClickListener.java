package com.hyperether.orderapp.listeners;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Toast;

import com.hyperether.orderapp.R;
import com.hyperether.orderapp.components.DataCache;
import com.hyperether.orderapp.components.Order;
import com.hyperether.orderapp.user.MenuList;
import com.hyperether.orderapp.user.OrderList;
import com.hyperether.orderapp.user.PromotionList;
import com.hyperether.orderapp.user.Reservation;

public class MainNavigationOnClickListener {

	private Activity mActivity = null;
	private final Context mContext;

	public MainNavigationOnClickListener(Activity activity, Context context) {
		super();
		this.mActivity = activity;
		this.mContext = context;
	}

	public OnClickListener reservationListener = new OnClickListener() {
		@Override
		public void onClick(View arg0) {
			DataCache.getInstance().getOrder().setOrderType(Order.ORDER_RESERVATION);
			Intent intent = new Intent(mContext, Reservation.class);
			mActivity.startActivity(intent);
		}
	};

	public OnClickListener promotionListener = new OnClickListener() {
		@Override
		public void onClick(View arg0) {
			Intent intent = new Intent(mContext, PromotionList.class);
			mActivity.startActivity(intent);
		}
	};

	public OnClickListener orderOverviewListener = new OnClickListener() {
		@Override
		public void onClick(View arg0) {
			Order mOrder = DataCache.getInstance().getOrder();
			if(!mOrder.getFoodList().isEmpty()){
				Intent intent = new Intent(mContext, OrderList.class);
				mActivity.startActivity(intent);
			}
			else{
				Toast.makeText(mContext, mContext.getString(R.string.text_order_empty), Toast.LENGTH_LONG).show();
			}
		}
	};

	public OnClickListener orderMenuListener = new OnClickListener() {
		@Override
		public void onClick(View arg0) {
			Intent intent = new Intent(mContext, MenuList.class);
			intent.putExtra("menuLevel", 0);
			intent.putExtra("supermenuId", 0);
			mActivity.startActivity(intent);
		}
	};

	public OnClickListener onAdListener = new OnClickListener() {
		@Override
		public void onClick(View arg0) {
			Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse("http://www.hyperether.com"));
			mActivity.startActivity(browserIntent);
		}
	};

}
