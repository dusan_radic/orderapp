package com.hyperether.orderapp.utils;

import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

import com.hyperether.orderapp.R;
import com.hyperether.orderapp.components.DataCache;

public class NotificationSettings extends Activity {
	public static final int SMS = 1;
	public static final int EMAIL = 2;
	public static final int WEBSERVICE = 3;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_notification_settings);

		Button bSMS = (Button) findViewById(R.id.radioSMS);
		bSMS.setVisibility(DataCache.getInstance().isPhone() ? View.VISIBLE : View.GONE);
	}

	public void onRadioButtonClicked(View view) {
		int mSendingType = 0;
		switch(view.getId()) {
		case R.id.radioSMS:
			mSendingType = SMS;
			break;
		case R.id.radioEmail:
			mSendingType = EMAIL;
			break;
		case R.id.radioWeb:
			mSendingType = WEBSERVICE;
			break;
		}

		SharedPreferences settings = getSharedPreferences(Preferences.PREF_FILE, 0);
		SharedPreferences.Editor editor = settings.edit();
		editor.putInt("notificationMethod", mSendingType);
		editor.commit();
		Intent resultIntent = new Intent();
		setResult(Activity.RESULT_FIRST_USER, resultIntent);
		finish();
	}
}
