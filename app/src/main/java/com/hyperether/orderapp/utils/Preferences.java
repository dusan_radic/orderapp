package com.hyperether.orderapp.utils;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.res.TypedArray;
import android.os.Build;
import android.util.DisplayMetrics;

public class Preferences {
	public static final String PREF_FILE = "DataPref.txt";
	public static final String PREF_LAST_MENU_ID = "lastMenuId";
	public static final String PREF_NOTIFICATION = "notificationMethod";
	public static final String PREF_IS_MANAGER = "isManager";

	public static final String DATABASE_NAME = "dbLocalOrderApp.db";
	public static final int DATABASE_VERSION = 1;

	public static final String LOG_TAG = "OrderApp";

	public static int getDisplayWidth(Activity activity){
		DisplayMetrics metrics = new DisplayMetrics();
		activity.getWindowManager().getDefaultDisplay().getMetrics(metrics);
		int usefulDisplayWidth = metrics.widthPixels - 32;
		return usefulDisplayWidth;
	}

	@SuppressLint("InlinedApi")
	public static int getDisplayHeight(Activity activity){
		DisplayMetrics metrics = new DisplayMetrics();
		activity.getWindowManager().getDefaultDisplay().getMetrics(metrics);
		int statusBarHeight = getStatusBarHeight(activity);

		int mActionBarSize = 0;
		if(Build.VERSION.SDK_INT >= 11){
			final TypedArray styledAttributes = activity.getTheme().obtainStyledAttributes(
					new int[] { android.R.attr.actionBarSize });
			mActionBarSize = (int) styledAttributes.getDimension(0, 0);
			styledAttributes.recycle();
		} else {
			/*
			 * TODO: Dusan
			 * Provali kako na tvom telefonu dobijas title/action bar
			 */
		}
		int usefulDisplayHeight = metrics.heightPixels - statusBarHeight - mActionBarSize - 32;
		return usefulDisplayHeight;
	}

	private static int getStatusBarHeight(Activity activity) {
		int result = 0;
		int resourceId = activity.getResources().getIdentifier("status_bar_height", "dimen", "android");
		if (resourceId > 0) {
			result = activity.getResources().getDimensionPixelSize(resourceId);
		}
		return result;
	}
}
