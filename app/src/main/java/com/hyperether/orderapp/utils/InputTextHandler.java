package com.hyperether.orderapp.utils;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;

import com.hyperether.orderapp.R;

@SuppressLint("Registered")
public class InputTextHandler extends Activity {

	private Button mButtonApprove;
	private EditText editText;

	@Override
	public void onCreate(Bundle savedInstanceState)	{
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_input_text_handler);

		editText = (EditText) findViewById(R.id.editText1);
		mButtonApprove = (Button) findViewById(R.id.buttonTextAccept);
		mButtonApprove.setOnClickListener(mButtonApproveListener);
	}

	private final OnClickListener mButtonApproveListener = new OnClickListener() {
		@Override
		public void onClick(View v) {
			String text=editText.getText().toString();
			Intent resultIntent = new Intent();
			resultIntent.putExtra("text", text);
			setResult(Activity.RESULT_OK, resultIntent);
			finish();
		}
	};

	// Save layout before stopping the activity
	@Override
	protected void onSaveInstanceState(Bundle outState) {
		outState.putString("textToSave", editText.getText().toString());
		super.onSaveInstanceState(outState);
	}

	// Restore layout after interruption
	@Override
	protected void onRestoreInstanceState(Bundle savedInstanceState) {
		super.onRestoreInstanceState(savedInstanceState);
		editText.setText(savedInstanceState.getString("textToSave"));
	}
}
