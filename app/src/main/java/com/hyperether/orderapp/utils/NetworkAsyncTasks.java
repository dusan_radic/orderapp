package com.hyperether.orderapp.utils;

import org.json.JSONObject;

import android.app.ProgressDialog;
import android.content.Context;
import android.os.AsyncTask;
import android.util.SparseArray;

import com.hyperether.orderapp.communication.ApiCalls;

public class NetworkAsyncTasks {
	Context mContext;

	public NetworkAsyncTasks(Context context, int lastMenuId){
		this.mContext = context;
	}

	/**
	 * This class is an asynchronous task
	 */
	public class ActiveMenuId extends AsyncTask<String, Integer, SparseArray<JSONObject>>{
		ProgressDialog mDialog = new ProgressDialog(mContext);

		@Override
		protected void onPreExecute() {
			mDialog.setMessage("Loading...");
			mDialog.setIndeterminate(true);
			mDialog.setCancelable(false);
			mDialog.show();
			super.onPreExecute();
		}

		@Override
		protected SparseArray<JSONObject> doInBackground(String... params) {
			return ApiCalls.getActiveMenuId(params[0]);
		}

		@Override
		protected void onPostExecute(SparseArray<JSONObject> result) {
			mDialog.dismiss();
			super.onPostExecute(result);
		}
	}
	/**
	 * This class is an asynchronous task and return data from server
	 * @return  menuItems- list which have all carte and food
	 * 
	 */
	public class MenuItems extends AsyncTask<String, Integer, SparseArray<JSONObject>>{
		@Override
		protected SparseArray<JSONObject> doInBackground(String... params) {
			return ApiCalls.getMenuItems(params[0]);
		}

		@Override
		protected void onPostExecute(SparseArray<JSONObject> result) {
		}
	}
}
