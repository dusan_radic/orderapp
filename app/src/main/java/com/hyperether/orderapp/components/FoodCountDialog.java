package com.hyperether.orderapp.components;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.view.Gravity;
import android.widget.EditText;

import com.hyperether.orderapp.adapters.FoodListMenuBaseAdapter;

public class FoodCountDialog {

	private final Context mContext;
	private final Food mFood;
	private final FoodListMenuBaseAdapter mAdapter;

	public FoodCountDialog (Context context, Food food, FoodListMenuBaseAdapter foodAdapter){
		this.mContext = context;
		this.mFood = new Food(food.getId(), food.getSupermenuId(), food.getText(), food.getDescription(),
				food.getPrice(), food.getImage());
		this.mAdapter = foodAdapter;
	}

	public void getFoodCount(){
		AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(mContext);
		/*alertDialogBuilder.setTitle("Your Title");*/
		final EditText input = new EditText(mContext);
		input.setInputType(2);//number
		input.setGravity(Gravity.CENTER);
		input.setText("1");
		alertDialogBuilder.setView(input);
		alertDialogBuilder
		.setMessage(String.valueOf(mFood.getText()))
		.setCancelable(false)
		.setPositiveButton("Ok",new DialogInterface.OnClickListener() {
			@Override
			public void onClick(DialogInterface dialog,int id) {
				int nIntFromET = 1;
				try {
					nIntFromET = Integer.parseInt(input.getText().toString());
				}
				catch (NumberFormatException e) {
					// handle the exception
				}
				if(nIntFromET > 0){
					mFood.setCount(nIntFromET);
					DataCache.getInstance().getOrder().addFood(mFood);
					DataCache.getInstance().getOrder().setStatus(Order.APP_DRAFT);
					mAdapter.notifyDataSetChanged();	// Update adapter that data set might have changed
				}
			}
		})
		.setNegativeButton("Cancel",new DialogInterface.OnClickListener() {
			@Override
			public void onClick(DialogInterface dialog,int id) {
				dialog.cancel();
			}
		});

		AlertDialog alertDialog = alertDialogBuilder.create();
		alertDialog.show();
	}
}
