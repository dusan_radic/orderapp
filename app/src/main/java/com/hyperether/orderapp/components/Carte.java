package com.hyperether.orderapp.components;

public class Carte extends Item {
	private int mHasSupermenu;
	private int mHasSubmenu;

	public Carte(){}

	public Carte( int id, int supermenuId, String text, int hasSupermenu, int hasSubmenu) {
		this.mId = id;
		this.mSupermenuId = supermenuId;
		this.mText = text;
		this.mHasSupermenu = hasSupermenu;
		this.mHasSubmenu = hasSubmenu;
	}

	public int hasSupermenu() {
		return this.mHasSupermenu;
	}

	public void setSupermenu(int hasSupermenu) {
		this.mHasSupermenu = hasSupermenu;
	}

	public int hasSubmenu() {
		return this.mHasSubmenu;
	}

	public void setSubmenu(int hasSubmenu) {
		this.mHasSubmenu = hasSubmenu;
	}

}
