package com.hyperether.orderapp.components;

import java.io.ByteArrayOutputStream;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.graphics.Bitmap;
import android.graphics.Bitmap.CompressFormat;
import android.graphics.BitmapFactory;
import android.util.Log;

import com.hyperether.orderapp.utils.Preferences;

public class DatabaseHelper extends SQLiteOpenHelper {

	private static DatabaseHelper dbInstance;
	private static final String TABLE_MENU = "orderMenu";
	private static final String TABLE_ORDERS_SENT = "orderSent";
	private static final String TABLE_ORDERS_CONFIRMED = "orderConfirmed";

	private static final int SENT_ORDER_FOOD_LIST = 1;
	private static final int CONFIRMED_ORDER_FOOD_LIST = 2;
	private static final int SENT_ORDER_FOOD_LIST_ACTIVE = 3;

	private static Context mContext;

	public DatabaseHelper(Context context) {
		super(context, Preferences.DATABASE_NAME, null, Preferences.DATABASE_VERSION);
	}

	public static DatabaseHelper getInstance(Context context){
		if(dbInstance==null){
			mContext = context;
			dbInstance = new DatabaseHelper(mContext);
			return dbInstance;
		} else
			return dbInstance;
	}

	private String sqlCreateTableMenu(){
		String sql = " orderMenu ("
				+ " hasSuperMenu int, "
				+ " hasSubMenu int, "
				+ " itemId integer primary key not null, "
				+ " itemText text, "
				+ " description text, "
				+ " price real, "
				+ " action int, "
				+ " image BLOB, "
				+ " superMenuId int);";
		return sql;
	}

	private String sqlCreateTableOrderItems(){
		String sql = " orderItems ("
				+ " orderId INT, "
				+ " menuitemid INT, "
				+ " count INT, "
				+ " manager INT);";
		return sql;
	}

	private String sqlCreateTableOrderSent(){
		String sql = " orderSent ("
				+ " orderId INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL,"
				+ " orderText TEXT, "
				+ " responseText TEXT, "
				+ " status INT, "
				+ " orderType INT, "
				+ " orderTime DATE, " //DateTime in milliseconds
				+ " numberOfPeople INT, "
				+ " address TEXT, "
				+ " keep INT, "
				+ " serverID INT, "
				+ " datetime INT); "; //DateTime in milliseconds
		return sql;
	}

	private String sqlCreateTableOrderConfirmed(){
		String sql = " orderConfirmed ("
				+ " orderId INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL,"
				+ " orderText TEXT, "
				+ " responseText TEXT, "
				+ " status INT, "
				+ " orderType INT, "
				+ " orderTime DATE, " //DateTime in milliseconds
				+ " numberOfPeople INT, "
				+ " address TEXT, "
				+ " keep INT, "
				+ " serverID INT, "
				+ " datetime INT); "; //DateTime in milliseconds
		return sql;
	}

	@Override
	public void onCreate(SQLiteDatabase db) {
		db.execSQL("CREATE TABLE IF NOT EXISTS " + sqlCreateTableMenu());
		db.execSQL("CREATE TABLE IF NOT EXISTS " + sqlCreateTableOrderSent());
		db.execSQL("CREATE TABLE IF NOT EXISTS " + sqlCreateTableOrderItems());
		db.execSQL("CREATE TABLE IF NOT EXISTS " + sqlCreateTableOrderConfirmed());
	}

	@Override
	public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
		// Drop older table if existed
		db.execSQL("DROP TABLE IF EXISTS " + TABLE_MENU);

		// Create tables again
		onCreate(db);
	}

	/*
	 * Order support functions
	 */
	private List<Order> getOrderList(String sql, int listType) {
		List<Order> orders = new ArrayList<Order>();
		SQLiteDatabase db = getReadableDatabase();
		Cursor cur = db.rawQuery(sql, null);
		try{
			if (cur != null )
				if  (cur.moveToFirst())
					do {
						Order order = new Order();
						order.setStatus(cur.getInt(cur.getColumnIndex("status")));
						int tableId = cur.getInt(cur.getColumnIndex("orderId"));
						if((order.getStatus() == Order.EMAIL) || (order.getStatus() == Order.SMS)) {
							order.setId(tableId);
						} else {
							order.setId(cur.getInt(cur.getColumnIndex("serverID")));
						}
						order.setOrderText(cur.getString(cur.getColumnIndex("orderText")));
						order.setResponseText(cur.getString(cur.getColumnIndex("responseText")));
						order.setOrderType(cur.getInt(cur.getColumnIndex("orderType")));
						long ordertime = cur.getLong(cur.getColumnIndex("orderTime"))*1000;
						order.setOrderTime(new Date(ordertime));//this because in database keep in seconds
						long timereq = cur.getLong(cur.getColumnIndex("datetime"))*1000;
						order.setTimeRequired(new Date(timereq));//this because in database keep in seconds
						order.setAddress(cur.getString(cur.getColumnIndex("address")));
						order.setNumberOfPeople(cur.getInt(cur.getColumnIndex("numberOfPeople")));
						order.setKeep(cur.getInt(cur.getColumnIndex("keep")));
						List<Food> food = getFoodList(tableId, listType);
						order.setFoodList(food);
						orders.add(order);
					}while (cur.moveToNext());
		}catch (Exception e) {
		}finally{
			cur.close();
			db.close();
		}
		return orders;
	}

	private String sqlSelectOrdersConfirmed(){
		return "select * from orderConfirmed ";
		//+ " where " +
		//we keep confirm order one day after timeRequired
		//" datetime>"+(new Date().getTime()-86400000)/1000
	}

	private String sqlSelectOrdersSent(){
		return "select * from orderSent where orderSent.status!= " + Order.APP_DRAFT;
		//+ " where " +
		//TODO add  correct number of days when we start to use application
		//" keep = 1  or  orderTime>"+(new Date().getTime()-432000000)/1000
	}

	private String sqlSelectOrdersDraft(){
		return "select * from orderSent where orderSent.status= " + Order.APP_DRAFT;
		//+ " where " +
		//TODO add  correct number of days when we start to use application
		//" keep = 1  or  orderTime>"+(new Date().getTime()-432000000)/1000
	}

	public List<Order> getOrderListConfirmed(){
		return getOrderList(sqlSelectOrdersConfirmed(), CONFIRMED_ORDER_FOOD_LIST);
	}

	public List<Order> getOrderListSent(){
		return getOrderList(sqlSelectOrdersSent(), SENT_ORDER_FOOD_LIST);
	}

	public List<Order> getOrderListDraft(){
		return getOrderList(sqlSelectOrdersDraft(), SENT_ORDER_FOOD_LIST_ACTIVE);
	}

	public List<Order> getOrderListSentActive(){
		return getOrderList(sqlSelectOrdersSent(), SENT_ORDER_FOOD_LIST_ACTIVE);
	}

	public Order getSentActiveOrderById(int orderId) {
		List<Order> activeSentOrders = getOrderListSentActive();
		Iterator<Order> iterator = activeSentOrders.iterator();
		Order order = null;
		while (iterator.hasNext()) {
			Order tmpOrder = iterator.next();
			if(tmpOrder.getId() == orderId){
				order = tmpOrder;
			}
		}
		return order;
	}

	public Order getDraftOrder(){
		Order dOrder = new Order();
		List<Order> ordersDraft = getOrderListDraft();
		if(!ordersDraft.isEmpty())
			dOrder =  ordersDraft.get(0);
		return dOrder;
	}

	public long insertOrderSent(Order order){
		return insertOrder(order, TABLE_ORDERS_SENT);
	}

	public long insertOrderConfirmed(Order order){
		return insertOrder(order, TABLE_ORDERS_CONFIRMED);
	}

	private long insertOrder(Order order, String table){
		long idInserted;
		SQLiteDatabase db = getWritableDatabase();
		ContentValues values = new ContentValues();
		int manager = 0;
		if(table.equals(TABLE_ORDERS_SENT)) {
			//values.put("orderText", "ORDER");
		}
		else if(table.equals(TABLE_ORDERS_CONFIRMED)){
			//values.put("orderText", order.getOrderText());
			manager = 1;
		}

		values.put("orderText", order.getOrderText());
		values.put("responseText", order.getResponseText());
		values.put("serverID", order.getId());
		values.put("status", order.getStatus());
		values.put("orderType", order.getOrderType());
		values.put("orderTime", order.getOrderTime()!=null ? order.getOrderTime().getTime()/1000 : System.currentTimeMillis()/1000);
		values.put("dateTime", order.getTimeRequired()!=null ? order.getTimeRequired().getTime()/1000 : System.currentTimeMillis()/1000);
		values.put("address", order.getAddress());
		values.put("keep", order.getKeep());
		values.put("numberOfPeople", order.getNumberOfPeople());

		try {
			idInserted = db.insert(table, null, values);
			if(order.getFoodList()!=null && !order.getFoodList().isEmpty())
				for(Food f :order.getFoodList()){
					ContentValues valuesFood = new ContentValues();
					valuesFood.put("orderId", idInserted);
					valuesFood.put("menuitemid", f.getId());
					valuesFood.put("count", f.getCount());
					valuesFood.put("manager", manager);
					db.insert("orderItems", null, valuesFood);
				}
		} catch (SQLException e) {
			idInserted = -1;
		}finally{
			db.close();
		}
		return idInserted;
	}

	public void updateSentOrder(int orderId, int status, String responseText ){
		SQLiteDatabase db = getWritableDatabase();
		ContentValues dataToUpdate = new ContentValues();
		dataToUpdate.put("status", status);
		dataToUpdate.put("responseText", responseText);
		try{
			db.update("orderSent", dataToUpdate, "serverID = " + orderId, null);
		}catch (Exception e){
			e.printStackTrace();
		}finally{
			db.close();
		}
	}

	public void updateKeepOrder(int orderId, int keep ){
		SQLiteDatabase db = getWritableDatabase();
		ContentValues dataToUpdate = new ContentValues();
		dataToUpdate.put("keep", keep);
		try{
			db.update("orderSent", dataToUpdate, "serverID = " + orderId, null);
		}catch (Exception e){
			e.printStackTrace();
		}finally{
			db.close();
		}
	}

	public boolean deleteSentOrder(int orderId, int orderStatus) {
		SQLiteDatabase db = getReadableDatabase();
		String sql1, sql2;

		if((orderStatus == Order.EMAIL) || (orderStatus == Order.SMS)) {
			sql1 = "delete from orderItems where orderItems.manager = 0 and orderItems.orderId = " + orderId + ";";
			sql2 = "delete from orderSent where orderSent.orderId= " + orderId + ";";
		} else {
			sql1 = "delete from orderItems where orderItems.manager = 0 and orderItems.orderId in (select orderSent.orderId from orderSent where orderSent.serverID = "+orderId+");";
			sql2 = "delete from orderSent where orderSent.serverID= " + orderId + ";";
		}

		try{
			db.execSQL(sql1);
			db.execSQL(sql2);
		}catch (Exception e) {
			return false;
		}finally{
			db.close();
		}
		return true;
	}

	public boolean deleteConfirmedOrder(int orderId) {
		SQLiteDatabase db = getReadableDatabase();
		String sql = "delete from orderItems where orderItems.manager = 1 and orderItems.orderId in (select orderConfirmed.orderId from orderConfirmed where orderConfirmed.serverID = "+orderId+");";
		try{
			db.execSQL(sql);
			sql = "delete from orderConfirmed where orderConfirmed.serverID= " + orderId + ";";
			db.execSQL(sql);
		}catch (Exception e) {
			return false;
		}finally{
			db.close();
		}
		return true;
	}

	public void deleteDraftOrder() {
		List<Order> ordersDraft = getOrderListDraft();
		if(!ordersDraft.isEmpty()) {
			deleteSentOrder(ordersDraft.get(0).getId(), Order.APP_DRAFT);
		}
	}

	/*
	 * Menu support functions
	 */
	/*
	 * Carte support functions
	 */

	public long insertCarte(Carte carte) {
		long idInserted=-1;
		SQLiteDatabase db = getWritableDatabase();
		ContentValues values = new ContentValues();
		values.put("hasSubMenu", carte.hasSubmenu());
		values.put("hasSuperMenu", carte.hasSupermenu());
		values.put("itemText", carte.getText());
		values.put("superMenuId", carte.getSupermenuId());
		values.put("itemId", carte.getId());
		if(carte.getImage()!=null)
			values.put("image",getBytes(carte.getImage()));
		try {
			idInserted = db.insert(TABLE_MENU, null, values);
		} catch (SQLException e) {
			idInserted = -1;
		}finally{
			db.close();
		}

		return idInserted;
	}

	public List<Carte> getCarteList(int supermenuId) {
		List<Carte> items = new ArrayList<Carte>();
		SQLiteDatabase db = getReadableDatabase();
		Cursor cur = db.rawQuery("SELECT * FROM " + TABLE_MENU + " WHERE " + "superMenuId" + "=" + supermenuId
				+" and hasSubMenu is not null", null);
		try{
			if (cur != null )
				if  (cur.moveToFirst())
					do {
						Carte item = new Carte();
						item.setId(cur.getInt(cur.getColumnIndex("itemId")));
						item.setText(cur.getString(cur.getColumnIndex("itemText")));
						item.setSupermenu(cur.getInt(cur.getColumnIndex("hasSuperMenu")));
						item.setSubmenu(cur.getInt(cur.getColumnIndex("hasSubMenu")));
						if(cur.getBlob(cur.getColumnIndex("image"))!=null)
							item.setImage(getImage(cur.getBlob(cur.getColumnIndex("image"))));
						items.add(item);
					}while (cur.moveToNext());
		}catch (Exception e) {
		}finally{
			cur.close(); // that's important too, otherwise you're gonna leak cursors
			db.close();
		}
		return items;
	}

	/*
	 * Food support functions
	 */
	/*
	 * Food support functions for Menu
	 */
	public long insertFoodMenu(Food food) {
		return insertFood(food, 0);
	}

	private long insertFood(Food food, int isAction) {
		long idInserted;
		SQLiteDatabase db = getWritableDatabase();
		ContentValues values = new ContentValues();
		values.put("description", food.getDescription());
		values.put("price", food.getPrice());
		values.put("itemText", food.getText());
		values.put("superMenuId", food.getSupermenuId());
		values.put("itemId", food.getId());
		values.put("action", isAction);
		if(food.getImage()!=null)
			values.put("image",getBytes(food.getImage()));

		try {
			idInserted = db.insert(TABLE_MENU, null, values);
		} catch (SQLException e) {
			idInserted = -1;
		}finally{
			db.close();
		}
		return idInserted;
	}

	private long updateFood(Food food, int isAction) {
		long idInserted;
		SQLiteDatabase db = getWritableDatabase();
		ContentValues values = new ContentValues();
		values.put("description", food.getDescription());
		values.put("price", food.getPrice());
		values.put("itemText", food.getText());
		values.put("superMenuId", food.getSupermenuId());
		values.put("itemId", food.getId());
		values.put("action", isAction);
		if(food.getImage()!=null)
			values.put("image",getBytes(food.getImage()));

		try {
			idInserted = db.update(TABLE_MENU, values, "orderMenu.itemId = " + food.getId(), null);
		} catch (SQLException e) {
			idInserted = -1;
		}finally{
			db.close();
		}
		return idInserted;
	}

	private List<Food> getFoodListMenu(String sql) {
		List<Food> items = new ArrayList<Food>();
		SQLiteDatabase db = getReadableDatabase();
		Cursor cur = db.rawQuery(sql, null);
		try{
			if (cur != null )
				if  (cur.moveToFirst())
					do {
						Food item = new Food();
						item.setId(cur.getInt(cur.getColumnIndex("itemId")));
						item.setText(cur.getString(cur.getColumnIndex("itemText")));
						item.setDescription(cur.getString(cur.getColumnIndex("description")));
						item.setPrice(cur.getDouble(cur.getColumnIndex("price")));
						if(cur.getBlob(cur.getColumnIndex("image"))!=null)
							item.setImage(getImage(cur.getBlob(cur.getColumnIndex("image"))));
						items.add(item);
					}while (cur.moveToNext());
		}catch (Exception e) {
		}finally{
			cur.close(); // that's important too, otherwise you're gonna leak cursors
			db.close();
		}
		return items;
	}

	private String sqlSelectMenuItemForId(int id){
		return "select * from orderMenu where orderMenu.itemId = " + id;
	}

	private String sqlSelectFoodList(int supermenuId){
		return "select * from orderMenu where " + "superMenuId" + "=" + supermenuId
				+ " and hasSubMenu is null";
	}

	public Food getFoodById(int foodId) {
		List<Food> foodList = getFoodListMenu(sqlSelectMenuItemForId(foodId));
		Food food = null;
		if(foodList != null && foodList.size()>0)
			food = foodList.get(0);
		return food;
	}

	public List<Food> getFoodListBySupermenuId(int supermenuId) {
		return getFoodListMenu(sqlSelectFoodList(supermenuId));
	}

	/*
	 * Food support functions for Order
	 */
	private List<Food> getFoodListOrder(String sql) {
		List<Food> items = new ArrayList<Food>();
		SQLiteDatabase db = getReadableDatabase();
		Cursor cur = db.rawQuery(sql, null);
		try{
			if (cur != null )
				if  (cur.moveToFirst())
					do {
						Food item = new Food();
						if(cur.getString(cur.getColumnIndex("itemText"))!=null){
							item.setId(cur.getInt(cur.getColumnIndex("menuitemid")));
							item.setText(cur.getString(cur.getColumnIndex("itemText")));
							item.setDescription(cur.getString(cur.getColumnIndex("description")));
							item.setCount(cur.getInt(cur.getColumnIndex("count")));
							item.setPrice(cur.getDouble(cur.getColumnIndex("price")));
							if(cur.getBlob(cur.getColumnIndex("image"))!=null)
								item.setImage(getImage(cur.getBlob(cur.getColumnIndex("image"))));
							items.add(item);
						}
						/*
						else{//old food we do not have in database
							item.setId(cur.getInt(cur.getColumnIndex("menuitemid")));
							item.setText(cur.getString(cur.getColumnIndex("itemText")));
							item.setDescription(cur.getString(cur.getColumnIndex("description")));
							item.setCount(cur.getInt(cur.getColumnIndex("count")));
							item.setPrice(0);
							items.add(item);
						}
						 */
					}while (cur.moveToNext());
		}catch (Exception e) {
		}finally{
			cur.close();
			db.close();
		}
		return items;
	}

	private String sqlSelectFoodListFromOldOrders(int orderId){
		/*
		return "select * from orderItems, orderSent LEFT OUTER JOIN orderMenu " +
				"ON orderMenu.itemId=orderItems.menuitemid where orderItems.manager = 0 and orderItems.orderId = orderSent.orderId and " +
				" orderSent.serverID = " + orderId;
		 */
		return "select * from orderItems, orderSent LEFT OUTER JOIN orderMenu " +
		"ON orderMenu.itemId=orderItems.menuitemid where orderItems.manager = 0 and orderItems.orderId = orderSent.orderId and " +
		" orderSent.orderId = " + orderId;
	}

	private String sqlSelectFoodListFromOldOrdersActive(int orderId){
		return "select * from orderItems, orderSent LEFT OUTER JOIN orderMenu " +
				"ON orderMenu.itemId=orderItems.menuitemid AND orderMenu.action != 2 where orderItems.manager = 0 and orderItems.orderId = orderSent.orderId and " +
				" orderSent.orderId = " + orderId;
	}

	private String sqlSelectFoodListFromConfirmedOrders(int orderId){
		return "select * from orderItems, orderConfirmed LEFT OUTER JOIN orderMenu " +
				"ON orderMenu.itemId=orderItems.menuitemid where orderItems.manager = 1 and orderItems.orderId = orderConfirmed.orderId and " +
				" orderConfirmed.orderId = " + orderId;
	}

	private List<Food> getFoodListOrdered(int orderId) {
		return getFoodListOrder(sqlSelectFoodListFromOldOrders(orderId));
	}

	private List<Food> getFoodListOrderedActive(int orderId) {
		return getFoodListOrder(sqlSelectFoodListFromOldOrdersActive(orderId));
	}

	private List<Food> getFoodListConfirmed(int orderId) {
		return getFoodListOrder(sqlSelectFoodListFromConfirmedOrders(orderId));
	}

	private List<Food> getFoodList(int orderId, int listType){
		switch(listType){
		case SENT_ORDER_FOOD_LIST:
			return getFoodListOrdered(orderId);
		case SENT_ORDER_FOOD_LIST_ACTIVE:
			return getFoodListOrderedActive(orderId);
		case CONFIRMED_ORDER_FOOD_LIST:
			return getFoodListConfirmed(orderId);
		}
		return null;
	}

	/*
	 * Promotion support functions
	 */
	public void updateFoodPromotionList(List<Food> foodList) {
		boolean choiceExist = false;
		SQLiteDatabase db = getWritableDatabase();

		/*
		 * First, clean current db food list - all inactive are set to 2
		 */
		Cursor cur = db.rawQuery("SELECT * FROM orderMenu WHERE orderMenu.action = 1", null);
		try{
			if (cur != null ) {
				if  (cur.moveToFirst()) {
					do {
						int id = cur.getInt(cur.getColumnIndex("itemId"));
						choiceExist = false;
						Iterator<Food> iterator = foodList.iterator();
						while (iterator.hasNext() && !choiceExist) {
							Food food = iterator.next();
							if(food.getId() == id){
								choiceExist = true;
							}
						}
						if(choiceExist == false){
							ContentValues dataToUpdate = new ContentValues();
							dataToUpdate.put("action", 2);
							try{
								db.update("orderMenu", dataToUpdate, "action = 1 AND itemId = " + id, null);
							}catch (Exception e){
								e.printStackTrace();
							}
						}
					}while (cur.moveToNext());
				}
			}
		}catch (Exception e) {
			Log.e(Preferences.LOG_TAG, "updateFoodPromotionList error");
		}finally{
			cur.close();
		}

		/*
		 * Second, update list of items with id that's active on server and add new items
		 */
		cur = db.rawQuery("SELECT * FROM orderMenu WHERE orderMenu.action = 1", null);
		try{
			for(Food f: foodList){
				choiceExist = false;
				if(cur != null ) {
					if(cur.moveToFirst()) {
						do {
							if(f.getId() == cur.getInt(cur.getColumnIndex("itemId"))){
								choiceExist = true;
							}
						}while(cur.moveToNext() && (choiceExist == false));
					}
				}
				if(choiceExist == false){
					insertFood(f, 1);
				} else {
					updateFood(f, 1);
				}
			}
		}catch (Exception e) {
			Log.e(Preferences.LOG_TAG, "updateFoodPromotionList error");
		}finally{
			cur.close();
			db.close();
		}
	}

	public void insertFoodPromotionList(List<Food> foodList) {
		deletePromotionList();
		for(Food f: foodList){
			insertFood(f, 1);
		}
	}

	public long insertFoodPromotion(Food food) {
		return insertFood(food, 1);
	}

	public void deletePromotionList() {
		SQLiteDatabase db = getWritableDatabase();
		String sql = "DELETE FROM " + TABLE_MENU + " WHERE action > 0";
		try {
			db.execSQL(sql);
		} catch (SQLException e) {

		}finally{
			db.close();
		}
	}

	/*
	 * Commonly used functions
	 */
	// convert from bitmap to byte array
	public static byte[] getBytes(Bitmap bitmap) {
		ByteArrayOutputStream stream = new ByteArrayOutputStream();
		bitmap.compress(CompressFormat.PNG, 0, stream);
		return stream.toByteArray();
	}

	// convert from byte array to bitmap
	public static Bitmap getImage(byte[] image) {
		return BitmapFactory.decodeByteArray(image, 0, image.length);
	}

	/*
	 * Not used
	 * 
	 */
	/*
	public void dbTableInit(String tableName) {
		SQLiteDatabase db;
		db = getReadableDatabase();
		Cursor cursor = db.rawQuery("SELECT name FROM sqlite_master WHERE type='table' AND name='orderMenu'", null);
		try{
			if(cursor!=null) {
				if(cursor.getCount()>0) {
					// table exist
				}
				else{
					db.close();
					db = getWritableDatabase();
					try {
						db.execSQL("DROP TABLE IF EXISTS orderMenu");
						db.execSQL(sqlCreateTable(tableName));
					} catch (SQLException e) {

					}
				}
			}
		}catch (Exception e) {

		}finally{
			cursor.close();
			db.close();
		}
	}

	public long dbTableUpdate(String tableName, Carte carte) {
		long idInserted = -1;
		boolean choiceExist = false;
		SQLiteDatabase db = getReadableDatabase();
		Cursor cur = db.rawQuery("SELECT " + "itemId" + " FROM " + tableName + " " , null);
		try{
			if (cur != null ) {
				if  (cur.moveToFirst()) {
					do {
						if((cur.getInt(cur.getColumnIndex("itemId")) == carte.getId())){
							choiceExist = true;
						}
					}while (cur.moveToNext() && (choiceExist == false));
				}
			}

			cur.close(); // that's important too, otherwise you're gonna leak cursors
			if(choiceExist == false){
				idInserted = dbItemInsert(tableName, carte);
			}
			else{
				//dbItemUpdate(tableName, item);
			}
		}catch (Exception e) {

		}finally{
			cur.close();
			db.close();
		}
		return idInserted;
	}

	public long dbTableUpdate(String tableName, Food food) {
		long idInserted = -1;
		boolean choiceExist = false;
		SQLiteDatabase db = getReadableDatabase();
		Cursor cur = db.rawQuery("SELECT " + "itemId" + " FROM " + tableName + " " , null);
		try{
			if (cur != null ) {
				if  (cur.moveToFirst()) {
					do {
						if((cur.getInt(cur.getColumnIndex("itemId")) == food.getId())){
							choiceExist = true;
						}
					}while (cur.moveToNext() && (choiceExist == false));
				}
			}

			cur.close(); // that's important too, otherwise you're gonna leak cursors
			if(choiceExist == false){
				idInserted = dbItemInsert(tableName, food);
			}
			else{
				//dbItemUpdate(tableName, item);
			}
		}catch (Exception e) {

		}finally{
			cur.close();
			db.close();
		}
		return idInserted;
	}

	public boolean dbTableCheckItemExist(String tableName, Item item) {
		boolean choiceExist = false;
		SQLiteDatabase db = getReadableDatabase();
		Cursor cur = db.rawQuery("SELECT " + "itemId" + " FROM " + tableName + " " , null);
		try{
			if (cur != null ) {
				if  (cur.moveToFirst()) {
					do {
						if((cur.getInt(cur.getColumnIndex("itemId")) == item.getId())){
							choiceExist = true;
						}
					}while (cur.moveToNext() && (choiceExist == false));
				}
			}

			cur.close(); // that's important too, otherwise you're gonna leak cursors
		}catch (Exception e) {

		}finally{
			cur.close();
			db.close();
		}
		return choiceExist;
	}

	public void dbTableDrop(String tableName) {
		SQLiteDatabase db = getWritableDatabase();
		String sql = "drop table " + tableName;
		try {
			db.execSQL(sql);
		} catch (SQLException e) {
		}finally{
			db.close();
		}
	}

	public void dbItemDelete(String tableName, Item item) {
		SQLiteDatabase db = getWritableDatabase();
		String sql = "DELETE FROM " + tableName + " WHERE " + "itemId" + "='" + item.getId() + "'";
		try {
			db.execSQL(sql);
		} catch (SQLException e) {

		}finally{
			db.close();
		}
	}

	public Carte dbSingleItemLoad(String tableName, int id) {
		Carte carte = new Carte();
		SQLiteDatabase db = getReadableDatabase();
		Cursor cur = db.rawQuery("SELECT * FROM " + tableName + " WHERE " + "itemId" + "=" + id, null);
		try{
			if (cur != null ) {
				if  (cur.moveToFirst()) {
					do {
						carte.setId(cur.getInt(cur.getColumnIndex("itemId")));
						carte.setText(cur.getString(cur.getColumnIndex("itemText")));
						carte.setSupermenu(cur.getInt(cur.getColumnIndex("hasSuperMenu")));
						carte.setSubmenu(cur.getInt(cur.getColumnIndex("hasSubMenu")));
						if(cur.getBlob(cur.getColumnIndex("image"))!=null)
							carte.setImage(getImage(cur.getBlob(cur.getColumnIndex("image"))));

					}while (cur.moveToNext());
				}
			}
		}catch (Exception e) {
		}finally{
			cur.close();
			db.close();
		}

		return carte;
	}
	 */
}
