package com.hyperether.orderapp.components;

import android.content.Context;
import android.graphics.Bitmap;
import android.os.Parcel;
import android.os.Parcelable;

import com.hyperether.orderapp.R;


public class Food extends Item implements Parcelable{
	private String mDescription;
	private double mPrice;
	private int mCount;

	public Food(){}

	public Food(int id, int supermenuId, String text, String description, double price, Bitmap image){
		this.mId = id;
		this.mSupermenuId = supermenuId;
		this.mText = text;
		this.mDescription = description;
		this.mPrice = price;
		this.mCount = 0;
		this.image = image;
	}

	public String getDescription() {
		return this.mDescription;
	}

	public void setDescription(String description) {
		this.mDescription = description;
	}

	public double getPrice() {
		return this.mPrice;
	}

	public String getPriceString(Context context) {
		return (mPrice + " " + context.getString(R.string.text_currency));
	}

	public void setPrice (double price){
		this.mPrice = price;
	}

	public int getCount(){
		return this.mCount;
	}

	public void setCount(int count){
		this.mCount= count;
	}

	private Food(Parcel source){
		this.mId = source.readInt();
		this.mText = source.readString();
		this.mDescription = source.readString();
		this.mSupermenuId = source.readInt();
		this.mPrice = source.readDouble();
		this.mCount = source.readInt();
		this.image = (Bitmap) source.readParcelable(Bitmap.class.getClassLoader());
	}

	@Override
	public int describeContents() {
		return 0;
	}

	@Override
	public void writeToParcel(Parcel dest, int flags) {
		dest.writeInt(mId);
		dest.writeString(mText);
		dest.writeString(mDescription);
		dest.writeInt(mSupermenuId);
		dest.writeDouble(mPrice);
		dest.writeInt(mCount);
		dest.writeParcelable(image, PARCELABLE_WRITE_RETURN_VALUE);
	}

	public static final Parcelable.Creator<Food> CREATOR = new Parcelable.Creator<Food>() {

		@Override
		public Food createFromParcel(Parcel source) {
			return new Food(source);
		}

		@Override
		public Food[] newArray(int size) {
			return new Food[size];
		}
	};
	/*
	@Override
	public String toString(Context context) {
		return this.mId+ ". " + this.mText + " (" +this.mDescription+")"+" [" + this.mPrice + " " + context.getString(R.string.text_currency) + "]";
	}
	 */
}