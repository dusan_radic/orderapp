package com.hyperether.orderapp.components;

import android.graphics.Bitmap;

public abstract class Item {
	protected int mId;
	protected int mSupermenuId;
	protected String mText;
	protected Bitmap image;

	public Item(){}

	public Item( int id, int supermenuId, String text) {
		this.mId = id;
		this.mSupermenuId = supermenuId;
		this.mText = text;
	}

	public int getId() {
		return this.mId;
	}

	public void setId(int id) {
		this.mId = id;
	}

	public int getSupermenuId() {
		return this.mSupermenuId;
	}

	public void setSupermenuId(int id) {
		this.mSupermenuId = id;
	}

	public String getText() {
		return this.mText;
	}

	public void setText(String text) {
		this.mText = text;
	}

	public Bitmap getImage() {
		return image;
	}

	public void setImage(Bitmap image) {
		this.image = image;
	}
	
	
}
