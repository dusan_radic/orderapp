package com.hyperether.orderapp.components;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import org.json.JSONException;
import org.json.JSONObject;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.SharedPreferences;
import android.provider.Settings.Secure;
import android.telephony.TelephonyManager;
import android.util.SparseArray;

import com.hyperether.orderapp.utils.Preferences;

public class DataCache{
	private static DataCache instance;

	private boolean isDataLoaded = false;
	private boolean isManager = false;
	private boolean isPhone = false;
	private int menuId = -1;
	private String imei = null;

	private Order mOrder = new Order();
	private Promotion mPromotion = new Promotion();

	private List<Order> mOrdersSent = new ArrayList<Order>();
	private List<Order> mOrdersConfirmed = new ArrayList<Order>();
	private List<Order> mOrdersPending = new ArrayList<Order>();
	@SuppressLint("UseSparseArrays")
	private SparseArray<Order> ordersForConfirm = null;

	/* A private Constructor prevents any other
	 * class from instantiating.
	 */
	private DataCache(){}

	/* Static 'instance' method */
	public static DataCache getInstance(){
		if(instance==null){
			instance = new DataCache();
			return instance;
		} else
			return instance;
	}

	/*
	 * Initialising data
	 */
	public void init(Context context){
		setImei(context);
		setDeviceState(context);
		loadConfirmedOrders(context);
		loadSentOrders(context);
		this.mOrder = DatabaseHelper.getInstance(context).getDraftOrder();
		SharedPreferences settings = context.getSharedPreferences(Preferences.PREF_FILE, 0);
		this.isManager = settings.getBoolean(Preferences.PREF_IS_MANAGER, false);
	}

	/**
	 * 	Load all orders from android database which manager confirmed.
	 * 
	 */
	public void loadConfirmedOrders(Context context){
		mOrdersConfirmed = DatabaseHelper.getInstance(context).getOrderListConfirmed();
	}

	/**
	 * 	Load all orders from android database which sent to web server.
	 * 
	 */
	public void loadSentOrders(Context context){
		mOrdersSent = DatabaseHelper.getInstance(context).getOrderListSent();
	}

	private void setImei(Context context) {
		TelephonyManager tm = (TelephonyManager) context.getSystemService(Context.TELEPHONY_SERVICE);
		String android_id = Secure.getString(context.getContentResolver(), Secure.ANDROID_ID);
		this.imei = (tm.getDeviceId() != null) ? tm.getDeviceId() : android_id;
	}

	public String getImei(Context context) {
		if(this.imei == null)
			setImei(context);
		return this.imei;
	}

	private void setDeviceState(Context context) {
		TelephonyManager tm = (TelephonyManager) context.getSystemService(Context.TELEPHONY_SERVICE);
		this.isPhone = tm.getPhoneType() == TelephonyManager.PHONE_TYPE_NONE ? false : true;
	}

	public boolean isPhone() {
		return this.isPhone;
	}

	public boolean isDataLoaded() {
		return isDataLoaded;
	}

	public void setDataLoaded(boolean isDataLoaded) {
		this.isDataLoaded = isDataLoaded;
	}

	public Order getOrder(){
		return this.mOrder;
	}

	public void setOrder(Order sourceOrder){
		Order order = new Order();
		List<Food> foodList = new ArrayList<Food>();
		for (Food f : sourceOrder.getFoodList()) {
			Food food = new Food(f.getId(), f.getSupermenuId(),
					f.getText(), f.getDescription(), f.getPrice(),
					f.getImage());
			food.setCount(f.getCount());
			foodList.add(food);
		}
		order.setFoodList(foodList);
		this.mOrder = order;
	}

	public void clearCurrentOrder(){
		this.mOrder = new Order();
	}

	public Promotion getPromotion(){
		return this.mPromotion;
	}

	public void setPromotion(Promotion promotion){
		this.mPromotion = promotion;
	}

	public void setActiveMenuId(SparseArray<JSONObject> result){
		JSONObject menuObject = result.get(0);
		if(menuObject!=null){
			try {
				this.menuId = menuObject.getInt("responde");
			} catch (JSONException e) {
				e.printStackTrace();
			}
		}
	}

	public int getActiveMenuId(){
		return this.menuId;
	}

	public void setManager(boolean value){
		this.isManager = value;
	}

	public boolean getManager(){
		return this.isManager;
	}

	/**
	 * Method that returns the old order list
	 * 
	 * @param context - calling context
	 * @return List of sent and confirmed orders
	 */
	public List<Order> getOrdersSent(Context context) {
		return mOrdersSent;
	}

	public Order getOrderSent(int orderId) {
		Iterator<Order> iterator = mOrdersSent.iterator();
		Order order = null;
		while (iterator.hasNext()) {
			Order tmpOrder = iterator.next();
			if(tmpOrder.getId() == orderId){
				order = tmpOrder;
			}
		}
		return order;
	}

	public void updateSentOrderStatus(int orderId, int status){
		Iterator<Order> iterator = mOrdersSent.iterator();
		while(iterator.hasNext()) {
			Order mOrder = iterator.next();
			if(mOrder.getId() == orderId) {
				mOrder.setStatus(status);
			}
		}
	}

	public List<Order> getOrdersPending() {
		return mOrdersPending;
	}

	public void setOrdersPending(List<Order> list) {
		this.mOrdersPending = list;
	}

	private void setOrdersPending() {
		mOrdersPending.clear();
		if(ordersForConfirm != null){
			for (int i = 0; i < ordersForConfirm.size(); i++) {
				int key = ordersForConfirm.keyAt(i);
				Order value = ordersForConfirm.get(key);
				mOrdersPending.add(value);
			}
		}
	}

	public SparseArray<Order> getOrdersForConfirm() {
		return ordersForConfirm;
	}

	public void setOrdersForConfirm(SparseArray<Order> orders) {
		this.ordersForConfirm = orders;
		setOrdersPending();
	}

	public void removeOrderFromConfirmMap(String sOrderId) {
		mOrdersConfirmed.add(ordersForConfirm.get(Integer.parseInt(sOrderId)));
		ordersForConfirm.remove(Integer.parseInt(sOrderId));
		setOrdersPending();
	}

	/**
	 * Method that returns the confirmed order list
	 * 
	 * @return List of sent and confirmed orders
	 */
	public List<Order> getOrdersConfirmed() {
		return mOrdersConfirmed;
	}

	public Order getOrderConfirmed(int orderId) {
		boolean found = false;
		Order mOrder = null;

		Iterator<Order> iterator = this.mOrdersConfirmed.iterator();
		while(iterator.hasNext()) {
			mOrder = iterator.next();
			if(mOrder.getId() == orderId) {
				found = true;
				break;
			}
		}

		if(found)
			return mOrder;
		else
			return null;
	}
}
