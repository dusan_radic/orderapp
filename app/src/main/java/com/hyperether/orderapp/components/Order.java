package com.hyperether.orderapp.components;

import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;

import android.annotation.SuppressLint;
import android.content.Context;
import android.os.Parcel;
import android.os.Parcelable;

import com.hyperether.orderapp.R;

/**
 * Class that stores the list of ordered food
 * 
 * @author Slobodan Prijic
 * @version February 2014
 *
 */
public class Order implements Parcelable {

	private int mId;
	private List<Food> foodList = new ArrayList<Food>();
	private double totalPrice = 0;
	private Date orderTime;
	private Date timeRequired;
	private int numberOfPeople = -1;
	private String address = null;
	private int keep = 0;
	private String orderText;
	private String mResponseText;

	/*
	 * Order types:
	 */
	private int orderType;
	public static final int ORDER_RESERVATION = 0;
	public static final int ORDER_FOOD_ONSITE = 1;
	public static final int ORDER_FOOD_PICKUP = 2;
	public static final int ORDER_FOOD_DELIVERY = 3;

	/*
	 * Order status:
	 */
	private int status = -2;
	public static final int SMS = 4;
	public static final int EMAIL = 2;
	public static final int APP_DRAFT = 3;
	public static final int APP_PENDING = -1;
	public static final int APP_APPROVED = 1;
	public static final int APP_DECLINED = 0;

	public Order(){}

	public Order(int mId, List<Food> foodList, double totalPrice, Date orderTime, Date timeRequired, int status) {
		this.mId = mId;
		this.foodList = foodList;
		this.totalPrice = totalPrice;
		this.orderTime = orderTime;
		this.timeRequired = timeRequired;
		this.status = status;
	}

	public int getId() {
		return this.mId;
	}

	public void setId(int id) {
		this.mId = id;
	}

	public List<Food> getFoodList() {
		return this.foodList;
	}

	public void addFood(Food newFood){
		boolean foodExist = false;
		Iterator<Food> iterator = foodList.iterator();

		while(iterator.hasNext()) {
			Food tmpFood = iterator.next();	// Get the next food from the order list
			if(tmpFood.getId() == newFood.getId()) { // if the id is equal to requested
				tmpFood.setCount(tmpFood.getCount() + newFood.getCount());
				totalPrice += newFood.getCount()*newFood.getPrice();
				foodExist = true;
				break;
			}
		}

		if(!foodExist){
			foodList.add(newFood);
			totalPrice += newFood.getCount()*newFood.getPrice();
		}
	}

	/**
	 * Method that removes the requested food from the order
	 * 
	 * @param id - unique food database id
	 */
	public void removeFood(int id) {

		Iterator<Food> iterator = foodList.iterator();

		while(iterator.hasNext()) {
			Food tmpFood = iterator.next();	// Get the next food from the order list
			if(tmpFood.getId() == id) { // if the id is equal to requested
				iterator.remove(); // delete the food from order
				totalPrice -= tmpFood.getCount()*tmpFood.getPrice();
			}
		}
	}

	public void setTotalPrice(double totalPrice) {
		this.totalPrice = totalPrice;
	}
	/**
	 * Method that returns current total price of an order
	 * 
	 * @return
	 */
	public double getTotalPrice() {
		return totalPrice;
	}

	public String getTotalPriceString(Context context) {
		String total = totalPrice + " " + context.getString(R.string.text_currency);
		return total;
	}

	/**
	 * Increment the quantity of the requested food by one
	 * 
	 * @param id
	 */
	public void incFoodQty(int id) {

		Iterator<Food> iterator = foodList.iterator();
		int tmpQty;

		while(iterator.hasNext()) {
			Food tmpFood = iterator.next();	// Get the next food from the order list
			if(tmpFood.getId() == id) { // if the id is equal to requested

				tmpQty = tmpFood.getCount(); // get the current food quantity
				tmpFood.setCount(++tmpQty);	 // increment and return
				totalPrice += tmpFood.getPrice(); //
			}
		}
	}

	/**
	 * Decrement the quantity of the requested food by one
	 * 
	 * @param id
	 */
	public void decFoodQty(int id) {

		Iterator<Food> iterator = foodList.iterator();
		int tmpQty;

		while(iterator.hasNext()) {
			Food tmpFood = iterator.next();	// Get the next food from the order list
			if(tmpFood.getId() == id) { // if the id is equal to requested

				tmpQty = tmpFood.getCount(); // get the current food quantity
				if(--tmpQty == 0) {  // There was only one piece of this Food so delet it
					iterator.remove(); // delete the food from order
					totalPrice -= tmpFood.getCount()*tmpFood.getPrice();
				} else {
					tmpFood.setCount(tmpQty);	 // increment and return
					totalPrice -= tmpFood.getPrice(); //
				}
			}
		}
	}

	public Date getOrderTime() {
		return orderTime;
	}

	public void setOrderTime(Date orderTime) {
		this.orderTime = orderTime;
	}

	public Date getTimeRequired() {
		return timeRequired;
	}

	public void setTimeRequired(Date timeRequired) {
		this.timeRequired = timeRequired;
	}

	public int getOrderType() {
		return orderType;
	}

	public void setOrderType(int orderType) {
		this.orderType = orderType;
	}

	public String getOrderTypeText(Context context) {
		String textOrderType = "";
		switch (orderType) {
		case ORDER_FOOD_DELIVERY:
			textOrderType = context.getString(R.string.text_delivery);
			break;
		case ORDER_FOOD_ONSITE:
			textOrderType = context.getString(R.string.text_reserve_table);
			break;
		case ORDER_FOOD_PICKUP:
			textOrderType = context.getString(R.string.text_pickup);
			break;
		case ORDER_RESERVATION:
			textOrderType = context.getString(R.string.text_reservation);
			break;
		default:
		}
		return textOrderType;
	}

	public int getStatus() {
		return status;
	}

	public String getStatusText(Context context) {
		String statusText = "";
		switch(this.status){
		case APP_PENDING:
			statusText = context.getString(R.string.text_pending);
			break;
		case APP_DECLINED:
			statusText = context.getString(R.string.text_cancelled);
			break;
		case APP_APPROVED:
			statusText = context.getString(R.string.text_approved);
			break;
		case APP_DRAFT:
			statusText = context.getString(R.string.text_draft);
			break;
		case SMS:
			statusText = context.getString(R.string.text_sms);
			break;
		case EMAIL:
			statusText = context.getString(R.string.text_email);
			break;
		}
		return statusText;
	}

	public void setStatus(int status) {
		this.status = status;
	}

	public void setFoodList(List<Food> foodList) {
		if(foodList!=null && !foodList.isEmpty() ) {
			for (Food f : foodList) {
				addFood(f);
			}
		}
	}

	public int getNumberOfPeople() {
		return numberOfPeople;
	}

	public void setNumberOfPeople(int numberOfPeople) {
		this.numberOfPeople = numberOfPeople;
	}

	@SuppressLint("SimpleDateFormat")
	public String toStringForConfirm(){

		String returnValue ="";
		switch (orderType) {
		case ORDER_FOOD_DELIVERY:{
			returnValue = returnValue+"DELIVERY\n\n";
			break;
		}
		case ORDER_FOOD_ONSITE:{
			returnValue = returnValue+"ONSITE\n\n";
			break;
		}
		case ORDER_FOOD_PICKUP:{
			returnValue = returnValue+"PICKUP\n\n";
			break;
		}
		case ORDER_RESERVATION:{
			returnValue = returnValue+"TABLE RESERVATION\n\n";
			break;
		}
		default:
			break;
		}
		returnValue = returnValue+orderText + "\n";
		return returnValue;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public int getKeep() {
		return keep;
	}

	public void setKeep(int keep) {
		this.keep = keep;
	}

	public String getOrderText() {
		return orderText;
	}

	public void setOrderText(String orderText) {
		this.orderText = orderText;
	}

	public String getResponseText() {
		return mResponseText;
	}

	public void setResponseText(String responseText) {
		this.mResponseText = responseText;
	}

	@Override
	public int describeContents() {
		return 0;
	}

	public Order(Parcel source) {
		this.mId = source.readInt();
		this.foodList = new ArrayList<Food>();
		source.readList(this.foodList,Food.class.getClassLoader());
		this.totalPrice = source.readDouble();
		this.orderTime =  new Date(source.readLong());
		this.timeRequired = new Date(source.readLong());
		this.status = source.readInt();
		this.numberOfPeople = source.readInt();
		this.address= source.readString();
		this.keep = source.readInt();
		this.orderText = source.readString();
		this.orderType = source.readInt();
		this.mResponseText = source.readString();
	}

	@Override
	public void writeToParcel(Parcel dest, int flags) {
		dest.writeInt(mId);
		dest.writeList(foodList);
		dest.writeDouble(totalPrice);
		dest.writeLong(orderTime.getTime());
		dest.writeLong(timeRequired.getTime());
		dest.writeInt(status);
		dest.writeInt(numberOfPeople);
		dest.writeString(address);
		dest.writeInt(keep);
		dest.writeString(orderText);
		dest.writeInt(orderType);
		dest.writeString(mResponseText);
	}

	public static final Parcelable.Creator<Order> CREATOR = new Parcelable.Creator<Order>() {

		@Override
		public Order createFromParcel(Parcel source) {
			return new Order(source);
		}

		@Override
		public Order[] newArray(int size) {
			return new Order[size];
		}
	};
}
