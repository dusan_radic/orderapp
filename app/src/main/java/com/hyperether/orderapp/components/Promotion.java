package com.hyperether.orderapp.components;

import java.util.ArrayList;
import java.util.List;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.util.Base64;
import android.util.SparseArray;

public class Promotion {
	private int mId;
	private final List<Food> mPromotionList = new ArrayList<Food>();

	public Promotion(){}

	public int getId() {
		return this.mId;
	}

	public void setId(int id) {
		this.mId = id;
	}

	public void addFood(Food newFood){
		mPromotionList.add(newFood);
	}

	public List<Food> getPromotionList(){
		return this.mPromotionList;
	}

	public void setPromotionList(SparseArray<JSONObject> result, Context context) {
		JSONObject promotionObject = result.get(1);
		if(promotionObject!=null){
			try {
				JSONArray list = promotionObject.getJSONArray("action");
				/**
				 * This is number in JSONArray which we get from server.
				 * This are columns for table menuitems					 *
				 *  0	-	id - long
				 *	1	-	name - string
				 *	2	-	description - string
				 *	3	-	supMenuId - long
				 *	4	-	price - double
				 *	5	-	action - boolean
				 *  6   -   icon - BLOB
				 */
				for (int i = 0; i < list.length(); i++) {
					JSONArray object = list.getJSONArray(i);
					Food food = new Food();
					food.setId(!object.get(0).equals(null) ? object.getInt(0)+10000 : i);
					food.setText(object.getString(1));
					food.setDescription(object.getString(2));
					food.setPrice(!object.get(4).equals(null) ? object.getDouble(4) : 0.0);
					food.setSupermenuId(!object.get(3).equals(null) ? object.getInt(3) : -1);
					byte[] decodedString = Base64.decode(object.getString(6).getBytes(), Base64.DEFAULT);
					Bitmap icon = BitmapFactory.decodeByteArray(decodedString, 0, decodedString.length);
					food.setImage(icon);
					mPromotionList.add(food);
				}
			} catch (JSONException e) {
				e.printStackTrace();
			}
		}
	}
}
