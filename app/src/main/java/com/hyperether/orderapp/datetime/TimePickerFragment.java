package com.hyperether.orderapp.datetime;

import java.util.Calendar;

import android.app.Dialog;
import android.app.TimePickerDialog;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.text.format.DateFormat;
import android.widget.TimePicker;

/**
 * This class serves as a dialog for picking a correct time without the possibility of
 * false input. When the time is set it is returned to the calling activity.
 * 
 * @author Dusan Radic
 * @version January 2014
 */
public class TimePickerFragment extends DialogFragment implements TimePickerDialog.OnTimeSetListener {

	TheListener listener;

	public interface TheListener{
		public void returnTime(String time);
	}

	@Override
	public Dialog onCreateDialog(Bundle savedInstanceState) {
		// Use the current time as the default values for the picker
		final Calendar c = Calendar.getInstance();
		int hour = c.get(Calendar.HOUR_OF_DAY);
		int minute = c.get(Calendar.MINUTE);
		listener = (TheListener) getActivity();

		// Create a new instance of TimePickerDialog and return it
		return new TimePickerDialog(getActivity(), this, hour, minute,
				DateFormat.is24HourFormat(getActivity()));
	}

	@Override
	public void onTimeSet(TimePicker view, int hourOfDay, int minute) {
		if (listener != null) {
			listener.returnTime(getFormattedTime(hourOfDay, minute));
		}
	}

	public String getFormattedTime(int hourOfDay, int minute) {
		return twoDigitString(hourOfDay) + ":" + twoDigitString(minute);
	}

	public String getFormattedTimeCurrent() {
		Calendar c = Calendar.getInstance();
		return twoDigitString(c.get(Calendar.HOUR_OF_DAY)) + ":" + twoDigitString(c.get(Calendar.MINUTE));
	}

	/**
	 * Formatting method for time output
	 * */
	private String twoDigitString(int number) {
		if (number == 0) {
			return "00";
		}

		if (number / 10 == 0) {
			return "0" + number;
		}

		return String.valueOf(number);
	}
}
