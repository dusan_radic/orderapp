package com.hyperether.orderapp.adapters;

import java.util.List;

import android.content.Context;
import android.graphics.Bitmap;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.hyperether.orderapp.R;
import com.hyperether.orderapp.components.Carte;

/**
 * Class that handles presentation of the list of menu categories.
 * 
 * @author Dusan Radic
 * @version February 7 2014
 *
 */
public class CarteBaseAdapter extends BaseAdapter {

	private List<Carte> mMenu = null;	

	private final Context context;

	public CarteBaseAdapter(Context context, List<Carte> mMenu) {

		this.context = context;
		this.mMenu = mMenu;
	}

	public void updateMenu(List<Carte> mMenu) {
		this.mMenu = mMenu;
		notifyDataSetChanged();
	}

	@Override
	public int getCount() {
		return mMenu.size();
	}

	/**
	 *  getItem(int) in Adapter returns Object but we can override
	 *  it to DbRoute thanks to Java return type covariance
	 */
	@Override
	public Object getItem(int position) {
		return mMenu.get(position);
	}

	/**
	 *  getItemId() is often useless, I think this should be the default
	 *  implementation in BaseAdapter
	 */
	@Override
	public long getItemId(int position) {
		// TODO Auto-generated method stub
		return position;
	}

	/**
	 * ViewHolder class stores important data for each displayed line
	 * 
	 * @author Dusan Radic
	 * @version February 7, 2014
	 *
	 */
	public class ViewHolder {
		public TextView textName; 
		public ImageView image;

	}

	/**
	 * Override of getView() method to implement specific layout
	 */
	@Override
	public View getView(int position, View convertView, ViewGroup parent) {

		TextView textName; 
		ImageView image;

		if (convertView == null) {
			convertView = LayoutInflater.from(context)
					.inflate(R.layout.list_element_carte, parent, false);

			ViewHolder holder = new ViewHolder();

			textName = (TextView) convertView.findViewById(R.id.textName);
			image = (ImageView) convertView.findViewById(R.id.icon);

			holder.textName = textName;
			holder.image = image;

			convertView.setTag(holder);
		} else {
			ViewHolder holder = (ViewHolder) convertView.getTag();

			textName = holder.textName;
			image = holder.image;
		}

		// Get the next carte
		Carte actCarte = mMenu.get(position);
		ViewHolder tmpHolder = (ViewHolder) convertView.getTag();
		convertView.setTag(tmpHolder);

		String name = actCarte.getText();
		Bitmap carteIcon = actCarte.getImage();

		textName.setText(name);

		if(carteIcon!=null){
			//TODO this I set because I get little icons if I do not set this
			carteIcon.setDensity(120);
			image.setImageBitmap(carteIcon);
		}  else {
			image.setImageResource(R.drawable.logo); // show the default icon
		}
		return convertView;
	}		

}
