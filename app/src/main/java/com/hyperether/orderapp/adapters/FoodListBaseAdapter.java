package com.hyperether.orderapp.adapters;

import java.util.List;

import android.content.Context;
import android.widget.BaseAdapter;

import com.hyperether.orderapp.components.Food;

/**
 * Class that handles presentation of the list of food elements.
 * 
 * @author Slobodan Prijic
 * @version 2014-04-14
 *
 */
public abstract class FoodListBaseAdapter extends BaseAdapter {

	protected List<Food> mFoodList = null;
	protected final Context context;

	public FoodListBaseAdapter(Context context, List<Food> foodList) {
		this.context = context;
		this.mFoodList = foodList;
	}

	public void updateFoodList(List<Food> foodList) {
		this.mFoodList = foodList;
		notifyDataSetChanged();
	}

	@Override
	public int getCount() {
		return mFoodList.size();
	}

	/**
	 *  getItem(int) in Adapter returns Object but we can override
	 *  it to Food thanks to Java return type covariance
	 */
	@Override
	public Object getItem(int position) {
		return mFoodList.get(position);
	}

	/**
	 *  getItemId() is often useless, I think this should be the default
	 *  implementation in BaseAdapter
	 */
	@Override
	public long getItemId(int position) {
		return position;
	}
}
