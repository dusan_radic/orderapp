package com.hyperether.orderapp.adapters;

import java.text.SimpleDateFormat;
import java.util.List;

import android.annotation.SuppressLint;
import android.content.Context;
import android.widget.BaseAdapter;

import com.hyperether.orderapp.components.Order;

public abstract class OrderListBaseAdapter extends BaseAdapter {
	protected List<Order> mOrderList = null;
	protected final Context mContext;

	@SuppressLint("SimpleDateFormat")
	protected SimpleDateFormat mDateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm");

	public OrderListBaseAdapter(Context context, List<Order> orderList) {
		this.mContext = context;
		this.mOrderList = orderList;
	}

	public void updateMenu(List<Order> orderList) {
		this.mOrderList = orderList;
		notifyDataSetChanged();
	}

	@Override
	public int getCount() {
		return mOrderList.size();
	}

	/**
	 *  getItem(int) in Adapter returns Object but we can override
	 *  it to Food thanks to Java return type covariance
	 */
	@Override
	public Object getItem(int position) {
		return mOrderList.get(position);
	}

	/**
	 *  getItemId() is often useless, I think this should be the default
	 *  implementation in BaseAdapter
	 */
	@Override
	public long getItemId(int position) {
		return position;
	}
}
