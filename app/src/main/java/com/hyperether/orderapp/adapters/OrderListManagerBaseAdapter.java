package com.hyperether.orderapp.adapters;

import java.util.List;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.hyperether.orderapp.R;
import com.hyperether.orderapp.components.DatabaseHelper;
import com.hyperether.orderapp.components.Order;

public class OrderListManagerBaseAdapter extends OrderListBaseAdapter {

	public OrderListManagerBaseAdapter(Context context, List<Order> orderList) {
		super(context, orderList);
	}

	/**
	 * ViewHolder class stores important data for each displayed line
	 * 
	 * @author Nebojsa Brankovic
	 * @version February 7, 2014
	 *
	 */
	public class ViewHolder {
		public TextView textDateOrder;
		public TextView textDateRequest;
		public TextView textPrice;
		public TextView textStatus;
		public TextView textOrderType;
		public int orderId;
	}

	/**
	 * Override of getView() method to implement specific layout
	 */
	@Override
	public View getView(int position, View convertView, ViewGroup parent) {

		TextView textDateOrder;
		TextView textDateRequest;
		TextView textPrice;
		TextView textStatus;
		TextView textOrderType;

		if (convertView == null) {
			convertView = LayoutInflater.from(mContext)
					.inflate(R.layout.list_element_manager_orders, parent, false);

			ViewHolder holder = new ViewHolder();

			textDateOrder = (TextView) convertView.findViewById(R.id.textDateOrder);
			textDateRequest = (TextView) convertView.findViewById(R.id.textDateRequest);
			textPrice = (TextView) convertView.findViewById(R.id.textOrderPrice);
			textStatus = (TextView) convertView.findViewById(R.id.textStatus);
			textOrderType = (TextView) convertView.findViewById(R.id.textOrderType);

			holder.textDateOrder = textDateOrder;
			holder.textDateRequest = textDateRequest;
			holder.textPrice = textPrice;
			holder.textStatus = textStatus;
			holder.textOrderType = textOrderType;

			convertView.setTag(holder);
		} else {
			ViewHolder holder = (ViewHolder) convertView.getTag();

			textDateOrder = holder.textDateOrder;
			textDateRequest = holder.textDateRequest;
			textPrice = holder.textPrice;
			textStatus = holder.textStatus;
			textOrderType = holder.textOrderType;
		}

		// Get the next food from the order
		Order actOrder = mOrderList.get(position);
		ViewHolder tmpHolder = (ViewHolder) convertView.getTag();	// Store the order id in this view's tag
		tmpHolder.orderId = actOrder.getId();
		convertView.setTag(tmpHolder);

		textDateOrder.setText(mDateFormat.format(actOrder.getOrderTime()));
		textDateRequest.setText(mDateFormat.format(actOrder.getTimeRequired()));
		textPrice.setText(actOrder.getTotalPriceString(mContext));
		textStatus.setText(actOrder.getStatusText(mContext));
		textOrderType.setText(actOrder.getOrderTypeText(mContext));

		return convertView;
	}

	public void remove(Object item) {
		mOrderList.remove(item);
		DatabaseHelper databaseHelper = new DatabaseHelper(mContext);
		databaseHelper.deleteConfirmedOrder(((Order)item).getId());
	}
}
