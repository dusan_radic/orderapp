package com.hyperether.orderapp.adapters;

import java.util.List;

import android.content.Context;
import android.graphics.Bitmap;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.hyperether.orderapp.R;
import com.hyperether.orderapp.components.Food;

/**
 * Class that handles presentation of the list of ordered items.
 * 
 * @author Dusan Radic
 * @version February 7 2014
 *
 */
public class FoodListOverviewBaseAdapter extends FoodListBaseAdapter {

	public FoodListOverviewBaseAdapter(Context context, List<Food> foodList) {
		super(context, foodList);
	}

	/**
	 * ViewHolder class stores important data for each displayed line
	 * 
	 * @author Dusan Radic
	 * @version February 7, 2014
	 *
	 */
	public class ViewHolder {
		public TextView textName;
		public TextView textPrice;
		public ImageView image;
		public TextView qty;
		public int id;
		public String fullName;
	}

	/**
	 * Override of getView() method to implement specific layout
	 */
	@Override
	public View getView(int position, View convertView, ViewGroup parent) {

		TextView textName;
		TextView textPrice;
		ImageView image;
		TextView qty;

		if (convertView == null) {
			convertView = LayoutInflater.from(context)
					.inflate(R.layout.list_element_manager_order, parent, false);

			ViewHolder holder = new ViewHolder();

			textName = (TextView) convertView.findViewById(R.id.textName);
			textPrice = (TextView) convertView.findViewById(R.id.textPrice);
			qty = (TextView) convertView.findViewById(R.id.textRealQty);
			image = (ImageView) convertView.findViewById(R.id.icon);

			holder.textName = textName;
			holder.textPrice = textPrice;
			holder.image = image;
			holder.qty = qty;

			convertView.setTag(holder);
		} else {
			ViewHolder holder = (ViewHolder) convertView.getTag();

			textName = holder.textName;
			textPrice = holder.textPrice;
			image = holder.image;
			qty = holder.qty;
		}

		// Get the next food from the order
		Food actFood = mFoodList.get(position);
		String name = actFood.getText();

		//store some data to facilitate line deletion
		ViewHolder tmpHolder = (ViewHolder) convertView.getTag();
		tmpHolder.id = actFood.getId(); // get and store the unique food id
		tmpHolder.fullName = name;
		convertView.setTag(tmpHolder);

		Integer quantity = actFood.getCount();
		Bitmap orderIcon = actFood.getImage();

		textName.setText(name);
		textPrice.setText(actFood.getPriceString(context));
		qty.setText(quantity.toString());

		if(orderIcon!=null){
			//TODO this I set because I get little icons if I do not set this
			orderIcon.setDensity(120);
			image.setImageBitmap(orderIcon);
		} else {
			image.setImageResource(R.drawable.logo); // show the default icon
		}

		return convertView;
	}
}
