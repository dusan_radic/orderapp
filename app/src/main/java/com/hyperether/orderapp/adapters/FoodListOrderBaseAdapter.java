package com.hyperether.orderapp.adapters;

import java.util.List;

import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.support.v4.content.LocalBroadcastManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;

import com.hyperether.orderapp.R;
import com.hyperether.orderapp.components.DataCache;
import com.hyperether.orderapp.components.Food;

/**
 * Class that handles presentation of the list of ordered items.
 * 
 * @author Dusan Radic
 * @version February 7 2014
 *
 */
public class FoodListOrderBaseAdapter extends FoodListBaseAdapter {

	public FoodListOrderBaseAdapter(Context context, List<Food> foodList) {
		super(context, foodList);
	}

	/**
	 * ViewHolder class stores important data for each displayed line
	 * 
	 * @author Dusan Radic
	 * @version February 7, 2014
	 *
	 */
	public class ViewHolder {
		public TextView textName;
		public TextView textPrice;
		public ImageView image;
		public TextView qty;
		public ImageButton addButton;
		public ImageButton remButton;
		public int id;
		public String fullName;
	}

	/**
	 * Override of getView() method to implement specific layout
	 */
	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		TextView textName;
		TextView textPrice;
		ImageView image;
		ImageButton addButton;
		ImageButton remButton;
		TextView qty;

		if (convertView == null) {
			convertView = LayoutInflater.from(context)
					.inflate(R.layout.list_element_order, parent, false);

			ViewHolder holder = new ViewHolder();

			textName = (TextView) convertView.findViewById(R.id.textName);
			textPrice = (TextView) convertView.findViewById(R.id.textPrice);
			qty = (TextView) convertView.findViewById(R.id.textRealQty);
			image = (ImageView) convertView.findViewById(R.id.icon);
			addButton = (ImageButton) convertView.findViewById(R.id.buttonOrderAdd);
			remButton = (ImageButton) convertView.findViewById(R.id.buttonOrderRemove);

			holder.textName = textName;
			holder.textPrice = textPrice;
			holder.image = image;
			holder.qty = qty;
			holder.addButton = addButton;
			holder.remButton = remButton;

			holder.addButton.setOnClickListener(addButtonListener);
			holder.remButton.setOnClickListener(remButtonListener);

			convertView.setTag(holder);
		} else {
			ViewHolder holder = (ViewHolder) convertView.getTag();

			textName = holder.textName;
			textPrice = holder.textPrice;
			image = holder.image;
			qty = holder.qty;
			addButton = holder.addButton;
			remButton = holder.remButton;
		}

		// Get the next food from the order
		Food actFood = mFoodList.get(position);

		//store some data to facilitate line deletion
		ViewHolder tmpHolder = (ViewHolder) convertView.getTag();
		tmpHolder.id = actFood.getId(); // get and store the unique food id
		tmpHolder.fullName = actFood.getText();
		convertView.setTag(tmpHolder);

		String name = actFood.getText();

		// If the string is too long shorten it
		//if(name.length() > 14)
		//	name = name.substring(0, 14) + " ...";

		Integer quantity = actFood.getCount();
		Bitmap orderIcon = actFood.getImage();

		textName.setText(name);
		textPrice.setText(actFood.getPriceString(context));
		qty.setText(quantity.toString());

		if(orderIcon!=null){
			//TODO this I set because I get little icons if I do not set this
			orderIcon.setDensity(120);
			image.setImageBitmap(orderIcon);
		} else {
			image.setImageResource(R.drawable.logo); // show the default icon
		}

		return convertView;
	}

	/**
	 * Interface that enables the BaseAdapter to notify the activity that data set is changed
	 * 
	 * @author Dusan Radic
	 * @version February 7 2014
	 *
	 */
	private OnDataChangedListener onDataChangedListener;

	public interface OnDataChangedListener {
		public void onDataChanged(Context context);
	}

	public void setOnDataChangedListener(OnDataChangedListener listener) {
		onDataChangedListener = listener;
	}

	/**
	 * In case that add button is pressed, open a dialog for confirmation
	 * 
	 */
	private final OnClickListener addButtonListener = new OnClickListener() {
		@Override
		public void onClick(View v) {

			View listElement = (View) v.getParent();	// getting the list element
			ViewHolder tmpHolder = (ViewHolder) listElement.getTag();	// get the tag for this element

			DataCache.getInstance().getOrder().incFoodQty(tmpHolder.id);	//Order.incFoodQty(tmpHolder.id);
			notifyDataSetChanged();

			// Create intent to inform the MenuList and PromotionList activities that the order has changed
			Intent msgIntent = new Intent("order-changed");
			LocalBroadcastManager.getInstance(context).sendBroadcast(msgIntent);

			// Notify the OrderOverviewList activity that data set is changed
			onDataChangedListener.onDataChanged(context);
		}
	};

	/**
	 * In case that remove button is pressed, open a dialog for confirmation
	 * 
	 */
	private final OnClickListener remButtonListener = new OnClickListener() {
		@Override
		public void onClick(View v) {

			View listElement = (View) v.getParent();	// getting the list element
			ViewHolder tmpHolder = (ViewHolder) listElement.getTag();	// get the tag for this element

			DataCache.getInstance().getOrder().decFoodQty(tmpHolder.id); //Order.decFoodQty(tmpHolder.id);
			notifyDataSetChanged();

			// Create intent to inform the MenuList and PromotionList activities that the order has changed
			Intent msgIntent = new Intent("order-changed");
			LocalBroadcastManager.getInstance(context).sendBroadcast(msgIntent);

			// Notify the OrderOverviewList activity that data set is changed
			onDataChangedListener.onDataChanged(context);
		}
	};

	public void remove(Object item) {
		mFoodList.remove(item);
	}

	public void clear() {
		mFoodList.removeAll(mFoodList);
	}
}
