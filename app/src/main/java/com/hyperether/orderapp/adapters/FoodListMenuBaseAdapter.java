package com.hyperether.orderapp.adapters;

import java.util.Iterator;
import java.util.List;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.hyperether.orderapp.R;
import com.hyperether.orderapp.components.DataCache;
import com.hyperether.orderapp.components.Food;

/**
 * Class that handles presentation of the list of food menu items.
 * 
 * @author Dusan Radic
 * @version February 7 2014
 *
 */
public class FoodListMenuBaseAdapter extends FoodListBaseAdapter {

	public FoodListMenuBaseAdapter(Context context, List<Food> foodList) {
		super(context, foodList);
	}

	/**
	 * ViewHolder class stores important data for each displayed line
	 * 
	 * @author Dusan Radic
	 * @version February 7, 2014
	 *
	 */
	public class ViewHolder {
		public TextView textName;
		public TextView textPrice;
		public ImageView image;
		public ImageView tag;
	}

	/**
	 * Override of getView() method to implement specific layout
	 */
	@Override
	public View getView(int position, View convertView, ViewGroup parent) {

		TextView textName;
		TextView textPrice;
		ImageView image;
		ImageView tag;

		if (convertView == null) { // first pass
			convertView = LayoutInflater.from(context)
					.inflate(R.layout.list_element, parent, false);

			ViewHolder holder = new ViewHolder();

			textName = (TextView) convertView.findViewById(R.id.textName);
			textPrice = (TextView) convertView.findViewById(R.id.textPrice);
			image = (ImageView) convertView.findViewById(R.id.icon);
			tag = (ImageView) convertView.findViewById(R.id.imageOkSign);

			holder.textName = textName;
			holder.textPrice = textPrice;
			holder.image = image;
			holder.tag = tag;

			convertView.setTag(holder);
		} else { // recycling
			ViewHolder holder = (ViewHolder) convertView.getTag();

			textName = holder.textName;
			textPrice = holder.textPrice;
			image = holder.image;
			tag = holder.tag;
		}

		// Get the next food
		Food actFood = mFoodList.get(position);

		if(DataCache.getInstance().getOrder() != null) { // if an order exists
			List<Food> orderedFood = DataCache.getInstance().getOrder().getFoodList(); // get the list of currently ordered food

			if(	!orderedFood.isEmpty()) { // if some food is ordered
				Iterator<Food> iterator = orderedFood.iterator(); // get the iterator for this food
				while (iterator.hasNext()) {
					Food tmpFood = iterator.next(); // get the next food from order
					if(actFood.getId() == tmpFood.getId()) { // If this food is found in the order tag it
						tag.setVisibility(View.VISIBLE);
						break;
					} else { // If not present, untag it
						tag.setVisibility(View.INVISIBLE);
					}
				}

			}
		}

		if(position % 2 != 0) { // color odd lines grey
			convertView.setBackgroundColor(Color.parseColor("#e1e1e1"));
		} else { // and even lines white
			convertView.setBackgroundColor(Color.parseColor("#ffffff"));
		}

		String name = actFood.getText();

		// If the string is too long shorten it
		if(name.length() > 30)
			name = name.substring(0, 30) + " ...";

		Bitmap foodIcon = actFood.getImage();

		textName.setText(name);
		textPrice.setText(actFood.getPriceString(context));
		if(foodIcon!=null){
			//TODO this I set because I get little icons if I do not set this
			foodIcon.setDensity(150);
			image.setImageBitmap(foodIcon);
		} else {
			image.setImageResource(R.drawable.logo); // show the default icon
		}

		return convertView;
	}
}
