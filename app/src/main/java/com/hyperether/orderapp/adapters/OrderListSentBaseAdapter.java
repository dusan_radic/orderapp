package com.hyperether.orderapp.adapters;

import java.util.Iterator;
import java.util.List;

import android.content.Context;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.hyperether.orderapp.R;
import com.hyperether.orderapp.components.DatabaseHelper;
import com.hyperether.orderapp.components.Food;
import com.hyperether.orderapp.components.Order;

public class OrderListSentBaseAdapter extends OrderListBaseAdapter {

	public OrderListSentBaseAdapter(Context context, List<Order> orderList) {
		super(context, orderList);
	}

	/**
	 * ViewHolder class stores important data for each displayed line
	 * 
	 * @author Dusan Radic
	 * @version February 7, 2014
	 *
	 */
	public class ViewHolder {
		public ImageView image;
		public TextView textFood;
		public TextView textPrice;
		public TextView textOrderTime;
		public TextView textStatus;
		public int orderId;
		public int orderStatus;
	}

	/**
	 * Override of getView() method to implement specific layout
	 */
	@Override
	public View getView(int position, View convertView, ViewGroup parent) {

		ImageView image;
		TextView textFood;
		TextView textPrice;
		TextView textOrderTime;
		TextView textStatus;

		if (convertView == null) {
			convertView = LayoutInflater.from(mContext)
					.inflate(R.layout.list_element_old_order, parent, false);

			ViewHolder holder = new ViewHolder();

			image = (ImageView) convertView.findViewById(R.id.icon);
			textFood = (TextView) convertView.findViewById(R.id.textFood);
			textPrice = (TextView) convertView.findViewById(R.id.textOrderPrice);
			textOrderTime = (TextView) convertView.findViewById(R.id.textOrderTime);
			textStatus = (TextView) convertView.findViewById(R.id.textStatus);

			holder.image = image;
			holder.textFood = textFood;
			holder.textPrice = textPrice;
			holder.textOrderTime = textOrderTime;
			holder.textStatus = textStatus;

			convertView.setTag(holder);
		} else {
			ViewHolder holder = (ViewHolder) convertView.getTag();

			image = holder.image;
			textFood = holder.textFood;
			textPrice = holder.textPrice;
			textOrderTime = holder.textOrderTime;
			textStatus = holder.textStatus;
		}

		// Get the next food from the order
		Order actOrder = mOrderList.get(position);
		ViewHolder tmpHolder = (ViewHolder) convertView.getTag();	// Store the order id in this view's tag
		tmpHolder.orderId = actOrder.getId();
		tmpHolder.orderStatus = actOrder.getStatus();
		convertView.setTag(tmpHolder);

		String orderText = "";
		if(actOrder.getOrderType() == Order.ORDER_RESERVATION){
			orderText = mContext.getString(R.string.text_reservation);
			textPrice.setText("");
		} else {
			//Searching for the most expensive food in the order to display
			List<Food> tmpFoodList = actOrder.getFoodList();
			Food tmpFood;
			Iterator<Food> iterator = tmpFoodList.iterator();
			int i = 0;

			// Get first three items from the order at max
			while(iterator.hasNext() && i++ < 3) {
				tmpFood = iterator.next();
				if(tmpFood.getText() != null)
					orderText += tmpFood.getText() + "; ";
			}
			textPrice.setText(actOrder.getTotalPriceString(mContext));
		}

		textFood.setText(orderText);
		textOrderTime.setText(mDateFormat.format(actOrder.getOrderTime()));
		textStatus.setText(actOrder.getStatusText(mContext));
		if(actOrder.getKeep()==1)
			textOrderTime.setBackgroundColor(Color.GREEN);
		return convertView;
	}

	public void remove(Object item) {
		mOrderList.remove(item);
		DatabaseHelper databaseHelper = new DatabaseHelper(mContext);
		databaseHelper.deleteSentOrder(((Order)item).getId(), ((Order)item).getStatus());
	}
}
