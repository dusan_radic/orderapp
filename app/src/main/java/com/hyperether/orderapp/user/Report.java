package com.hyperether.orderapp.user;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.NotificationManager;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;

import com.hyperether.orderapp.R;
import com.hyperether.orderapp.communication.NotificationService;
import com.hyperether.orderapp.communication.OrderInsertService;
import com.hyperether.orderapp.components.DataCache;
import com.hyperether.orderapp.components.Order;
import com.hyperether.orderapp.utils.Preferences;

@SuppressLint("SimpleDateFormat")
public class Report extends Activity implements OnClickListener{

	public static String REPORT_TYPE = "report_type";
	public static final int REPORT_RESERVATON = 1;
	public static final int REPORT_NOTIFICATION = 2;
	public static final int REPORT_INSERTION = 3;
	public static String REPORT_ORDER_ID = "report_order_id";

	private Button mButtonClose, mButtonDelete;
	private EditText mTextReport;
	private int orderId;
	private int positionInParent;
	Order mOrder;
	NotificationManager mNotificationManager;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_report);

		mTextReport = (EditText) findViewById(R.id.textReport);
		mTextReport.setFocusable(false);
		mButtonClose = (Button) findViewById(R.id.buttonClose);
		mButtonClose.setOnClickListener(this);
		mButtonDelete = (Button) findViewById(R.id.buttonDelete);

		Bundle b = getIntent().getExtras();
		int reportType = b.getInt(REPORT_TYPE, -1);
		switch (reportType){
		case REPORT_RESERVATON:
			mButtonClose.setWidth(getResources().getDimensionPixelSize(R.dimen.button_preffered_width));
			mButtonDelete.setWidth(getResources().getDimensionPixelSize(R.dimen.button_preffered_width));
			orderId = b.getInt("OLD_ORDER_ID");
			positionInParent = b.getInt("OLD_ORDER_POS");
			mOrder = DataCache.getInstance().getOrderSent(orderId);
			/*
			 * Protection: can't delete order until server respond
			 */
			if(mOrder.getStatus() != Order.APP_PENDING) {
				mButtonDelete.setOnClickListener(this);
			}
			break;
		case REPORT_NOTIFICATION:
			mButtonDelete.setVisibility(View.GONE);
			mButtonClose.setWidth(Preferences.getDisplayWidth(this));
			orderId = b.getInt(REPORT_ORDER_ID, -1);
			mOrder = DataCache.getInstance().getOrderSent(orderId);
			mNotificationManager =
					(NotificationManager) this.getSystemService(NOTIFICATION_SERVICE);
			mNotificationManager.cancel(NotificationService.NOTIFICATION_ID);
			break;
		case REPORT_INSERTION:
			mButtonDelete.setVisibility(View.GONE);
			mButtonClose.setWidth(Preferences.getDisplayWidth(this));
			orderId = b.getInt(REPORT_ORDER_ID, -1);
			mOrder = DataCache.getInstance().getOrderSent(orderId);
			mNotificationManager =
					(NotificationManager) this.getSystemService(NOTIFICATION_SERVICE);
			mNotificationManager.cancel(OrderInsertService.NOTIFICATION_ID);
			break;
		}

		mTextReport.setText(generateReport(mOrder));
	}

	@Override
	public void onClick(View v) {
		switch (v.getId()){
		case R.id.buttonClose:
			Intent resultIntent = new Intent();
			setResult(Activity.RESULT_OK, resultIntent);
			finish();
			break;
		case R.id.buttonDelete:
			resultIntent = new Intent();
			resultIntent.putExtra("OLD_ORDER_POS", positionInParent);
			resultIntent.putExtra("OLD_ORDER_ID", orderId);
			setResult(Activity.RESULT_FIRST_USER, resultIntent);
			finish();
			break;
		}
	}

	private String generateReport(Order order){
		String report = "";
		if(order.getOrderText() != null){
			report += order.getOrderText();
		}
		report += "\n" + this.getString(R.string.text_status) + " " + order.getStatusText(this);
		if(order.getResponseText() != null){
			report += "\n\n" + this.getString(R.string.text_manager_response);
			report += order.getResponseText();
		}
		return report;
	}
}
