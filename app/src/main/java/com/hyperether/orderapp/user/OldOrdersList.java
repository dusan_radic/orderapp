package com.hyperether.orderapp.user;

import java.util.List;

import android.app.Activity;
import android.app.ListActivity;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.view.View;
import android.widget.ListView;

import com.hyperether.orderapp.R;
import com.hyperether.orderapp.adapters.OrderListSentBaseAdapter;
import com.hyperether.orderapp.components.DataCache;
import com.hyperether.orderapp.components.Order;
import com.hyperether.orderapp.listeners.SwipeDismissListViewTouchListener;

/**
 * Class that presents the all previous orders
 * 
 * @version March, 2014
 *
 */
public class OldOrdersList extends ListActivity {
	private static final int ORDER_NAVIGATION = 1;
	private OrderListSentBaseAdapter adapter;
	List<Order> sentOrders;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_element_list);

		loadData();
		createList();

		if(Build.VERSION.SDK_INT >= 12){
			ListView listView = getListView();
			// Create a ListView-specific touch listener. ListViews are given special treatment because
			// by default they handle touches for their list items... i.e. they're in charge of drawing
			// the pressed state (the list selector), handling list item clicks, etc.
			SwipeDismissListViewTouchListener touchListener = new SwipeDismissListViewTouchListener(
					listView,
					new SwipeDismissListViewTouchListener.DismissCallbacks() {
						@Override
						public boolean canDismiss(int position) {
							return true;
						}

						@Override
						public void onDismiss(ListView listView, int[] reverseSortedPositions) {
							for (int position : reverseSortedPositions) {
								adapter.remove(adapter.getItem(position));
							}
							adapter.notifyDataSetChanged();
						}
					});
			listView.setOnTouchListener(touchListener);
			// Setting this scroll listener is required to ensure that during ListView scrolling,
			// we don't look for swipes.
			listView.setOnScrollListener(touchListener.makeScrollListener());
		}
	}

	private void loadData(){
		sentOrders = DataCache.getInstance().getOrdersSent(this);
	}

	private void createList(){
		ListView list = (ListView) findViewById(android.R.id.list);
		adapter = new OrderListSentBaseAdapter(this, sentOrders);
		if(adapter.getCount() > 0) {
			list.setAdapter(adapter);
		}
	}

	@Override
	protected void onListItemClick(ListView l, View v, int position, long id) {
		OrderListSentBaseAdapter.ViewHolder tmpHolder = (OrderListSentBaseAdapter.ViewHolder) v.getTag();

		Order mOrder = DataCache.getInstance().getOrderSent(tmpHolder.orderId);
		if(mOrder.getOrderType() == Order.ORDER_RESERVATION){
			Intent intent = new Intent(this, Report.class);
			intent.putExtra(Report.REPORT_TYPE, Report.REPORT_RESERVATON);
			intent.putExtra("OLD_ORDER_ID", tmpHolder.orderId);
			intent.putExtra("OLD_ORDER_POS", position);
			startActivityForResult(intent, ORDER_NAVIGATION);
		} else {
			// Passing the unique database id of the chosen route in order to draw it correctly
			Intent intent = new Intent(this, OldOrderList.class);
			intent.putExtra("OLD_ORDER_ID", tmpHolder.orderId);
			intent.putExtra("OLD_ORDER_POS", position);
			startActivityForResult(intent, ORDER_NAVIGATION);
		}
	}

	@Override
	public void onActivityResult(int requestCode, int resultCode, Intent data) {
		super.onActivityResult(requestCode, resultCode, data);
		switch(requestCode) {
		case (ORDER_NAVIGATION) :
			if (resultCode == OldOrderList.RESULT_FINISH) {
				Intent resultIntent = new Intent();
				setResult(Activity.RESULT_OK, resultIntent);
				finish();
			} else if (resultCode == Activity.RESULT_FIRST_USER) {
				if(data.hasExtra("OLD_ORDER_POS")){
					int pos = data.getExtras().getInt("OLD_ORDER_POS");
					if(pos >= 0){
						adapter.remove(adapter.getItem(pos));
						adapter.notifyDataSetChanged();
					}
					DataCache.getInstance().loadSentOrders(this);
					sentOrders = DataCache.getInstance().getOrdersSent(this);
					if(sentOrders.isEmpty()){
						finish();
					}
				}
			}
		break;
		}
	}
}
