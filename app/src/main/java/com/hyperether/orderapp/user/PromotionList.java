package com.hyperether.orderapp.user;

import java.util.List;

import android.app.Activity;
import android.app.ListActivity;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.support.v4.content.LocalBroadcastManager;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ListView;
import android.widget.Toast;

import com.hyperether.orderapp.R;
import com.hyperether.orderapp.adapters.FoodListMenuBaseAdapter;
import com.hyperether.orderapp.components.DataCache;
import com.hyperether.orderapp.components.Food;
import com.hyperether.orderapp.components.FoodCountDialog;

/**
 * Class that presents the Today's Promotion offer
 *
 * @version March, 2014
 */
public class PromotionList extends ListActivity {
    private static final int ORDER_NAVIGATION = 1;
    private FoodListMenuBaseAdapter adapter;
    private List<Food> mPromotionList = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_element_list);

        loadData();
        createList();
    }

    private void loadData() {
        mPromotionList = DataCache.getInstance().getPromotion().getPromotionList();
    }

    /**
     * Insert data into the custom adapter and show it
     */
    private void createList() {
        ListView list = (ListView) findViewById(android.R.id.list);
        adapter = new FoodListMenuBaseAdapter(PromotionList.this, mPromotionList);
        if (adapter.getCount() > 0) {
            list.setAdapter(adapter);
        } else {
            Toast.makeText(this, this.getString(R.string.text_no_promotions), Toast.LENGTH_LONG).show();
        }
    }

    /**
     * Monitor for the click on one row of the ListView
     */
    @Override
    protected void onListItemClick(ListView l, View v, int position, long id) {
        FoodCountDialog dialog = new FoodCountDialog(this, (Food) adapter.getItem(position), adapter);
        dialog.getFoodCount();
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        switch (requestCode) {
            case (ORDER_NAVIGATION):
                if (resultCode == Activity.RESULT_OK) {
                    Intent resultIntent = new Intent();
                    setResult(Activity.RESULT_OK, resultIntent);
                    finish();
                }
                break;
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.order_promotion, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        Intent intent;
        switch (item.getItemId()) {
            case R.id.action_order_overview:
                intent = new Intent(this, OrderList.class);
                startActivityForResult(intent, ORDER_NAVIGATION);
                return true;
            case R.id.action_order_menu:
                intent = new Intent(this, MenuList.class);
                intent.putExtra("menuLevel", 0);
                intent.putExtra("supermenuId", 0);
                startActivityForResult(intent, ORDER_NAVIGATION);
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    // handler for received Intents for the "order-changed" event
    private final BroadcastReceiver mMessageReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            // Refresh the list data and update adapter
            adapter.notifyDataSetChanged();
        }
    };

    @Override
    protected void onResume() {
        super.onResume();
        createList();
        // Register mMessageReceiver to receive messages that the order has changed
        LocalBroadcastManager.getInstance(this).registerReceiver(mMessageReceiver,
                new IntentFilter("order-changed"));
    }

    @Override
    protected void onPause() {
        // Unregister since the activity is not visible
        LocalBroadcastManager.getInstance(this).unregisterReceiver(mMessageReceiver);
        super.onPause();
    }
}
