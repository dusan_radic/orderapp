package com.hyperether.orderapp.user;

import android.app.Activity;
import android.app.ListActivity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.hyperether.orderapp.R;
import com.hyperether.orderapp.adapters.FoodListOverviewBaseAdapter;
import com.hyperether.orderapp.adapters.FoodListOverviewBaseAdapter.ViewHolder;
import com.hyperether.orderapp.components.DataCache;
import com.hyperether.orderapp.components.DatabaseHelper;
import com.hyperether.orderapp.components.Order;

/**
 * Class that presents the single previous order
 *
 * @version March, 2014
 */
public class OldOrderList extends ListActivity implements OnClickListener {
    private static final int ORDER_NAVIGATION = 1;
    public static final int RESULT_FINISH = 2;
    private FoodListOverviewBaseAdapter adapter;
    private Order mOrder = null;
    TextView currentPrice;
    private int orderId;
    private int positionInParent;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_old_order_list);

        Bundle b = getIntent().getExtras();
        orderId = b.getInt("OLD_ORDER_ID");
        positionInParent = b.getInt("OLD_ORDER_POS");

        loadData();
        createList();
        createConfirmLayer();
    }

    private void loadData() {
        mOrder = DataCache.getInstance().getOrderSent(orderId);
    }

    private void createList() {
        ListView list = (ListView) findViewById(android.R.id.list);
        adapter = new FoodListOverviewBaseAdapter(this, mOrder.getFoodList());
        if (adapter.getCount() > 0) {
            list.setAdapter(adapter);
        }

        currentPrice = (TextView) findViewById(R.id.textCurrentTotalPrice);
        currentPrice.setText(mOrder.getTotalPriceString(this));
    }

    /**
     * Setup button listeners
     */
    private void createConfirmLayer() {
        Button buttonClose = (Button) findViewById(R.id.buttonClose);
        buttonClose.setOnClickListener(this);
        buttonClose.setText(R.string.button_close);

        Button buttonEdit = (Button) findViewById(R.id.buttonEdit);
        buttonEdit.setOnClickListener(this);
        buttonEdit.setText(R.string.button_edit);

        Button buttonRemove = (Button) findViewById(R.id.buttonDelete);
        buttonRemove.setText(R.string.button_delete);
        /*
		 * Protection: can't delete order until server respond
		 */
        if (mOrder.getStatus() != Order.APP_PENDING) {
            buttonRemove.setOnClickListener(this);
        }
    }

    /**
     * Monitor for the click on one row of the ListView
     */
    @Override
    protected void onListItemClick(ListView l, View v, int position, long id) {
        ViewHolder tmpHolder = (ViewHolder) v.getTag();    // get the tag for this element
        String name = tmpHolder.fullName;
        Toast.makeText(this, name, Toast.LENGTH_LONG).show();
    }

    @Override
    public void onClick(View v) {
        Intent resultIntent;
        switch (v.getId()) {
            case R.id.buttonClose:
                resultIntent = new Intent();
                setResult(Activity.RESULT_OK, resultIntent);
                finish();
                break;
            case R.id.buttonEdit:
                // Edit
                if (mOrder != null && mOrder.getFoodList() != null && !mOrder.getFoodList().isEmpty()) {
                    mOrder = DatabaseHelper.getInstance(this).getSentActiveOrderById(mOrder.getId());
                    DataCache.getInstance().setOrder(mOrder);
                    DataCache.getInstance().getOrder().setStatus(Order.APP_DRAFT);

                    Intent intent = new Intent(this, OrderList.class);
                    startActivityForResult(intent, ORDER_NAVIGATION);
                } else {
                    Toast.makeText(this, this.getString(R.string.text_order_empty), Toast.LENGTH_LONG).show();
                }
                break;
            case R.id.buttonDelete:
                resultIntent = new Intent();
                resultIntent.putExtra("OLD_ORDER_POS", positionInParent);
                resultIntent.putExtra("OLD_ORDER_ID", orderId);
                setResult(Activity.RESULT_FIRST_USER, resultIntent);
                finish();
            default:
                break;
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        switch (requestCode) {
            case (ORDER_NAVIGATION):
                if (resultCode == Activity.RESULT_OK) {
                    Intent resultIntent = new Intent();
                    setResult(RESULT_FINISH, resultIntent);
                    finish();
                } else if (resultCode == Activity.RESULT_CANCELED) {

                }
                break;
        }
    }
	/*
	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		getSupportMenuInflater().inflate(R.menu.order_keep, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()) {
		case R.id.keepOrder:{
			DatabaseHelper databaseHelper = DatabaseHelper.getInstance(getApplicationContext());
			databaseHelper.updateKeepOrder(mOrder.getId(), 1);
			mOrder.setKeep(1);
			return true;
		}
		case R.id.notKeepOrder:{
			DatabaseHelper databaseHelper = DatabaseHelper.getInstance(getApplicationContext());
			databaseHelper.updateKeepOrder(mOrder.getId(), 0);
			mOrder.setKeep(0);
			return true;
		}
		default:
			return super.onOptionsItemSelected(item);
		}
	}
	 */
}
