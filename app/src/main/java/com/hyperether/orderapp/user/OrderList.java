package com.hyperether.orderapp.user;

import android.app.Activity;
import android.app.ListActivity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.hyperether.orderapp.R;
import com.hyperether.orderapp.adapters.FoodListOrderBaseAdapter;
import com.hyperether.orderapp.adapters.FoodListOrderBaseAdapter.OnDataChangedListener;
import com.hyperether.orderapp.adapters.FoodListOrderBaseAdapter.ViewHolder;
import com.hyperether.orderapp.components.DataCache;
import com.hyperether.orderapp.components.DatabaseHelper;
import com.hyperether.orderapp.components.Order;

/**
 * Class that presents the current order
 *
 * @version March, 2014
 */
public class OrderList extends ListActivity implements OnClickListener {
    private static final int ORDER_NAVIGATION = 1;
    private FoodListOrderBaseAdapter adapter;
    private Order mOrder = null;
    TextView currentPrice;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_order_list);

        loadData();
        createList();
        createConfirmLayer();
    }

    private void loadData() {
        mOrder = DataCache.getInstance().getOrder();
    }

    private void createList() {
        ListView list = (ListView) findViewById(android.R.id.list);
        adapter = new FoodListOrderBaseAdapter(this, mOrder.getFoodList());
        if (adapter.getCount() > 0) {
            list.setAdapter(adapter);
        }

        currentPrice = (TextView) findViewById(R.id.textCurrentTotalPrice);
        currentPrice.setText(mOrder.getTotalPriceString(this));

        // Listening for data set change
        adapter.setOnDataChangedListener(new OnDataChangedListener() {
            @Override
            public void onDataChanged(Context context) {
                currentPrice.setText(mOrder.getTotalPriceString(context));
            }
        });
    }

    /**
     * Setup button listeners
     */
    private void createConfirmLayer() {
        Button buttonConfirm = (Button) findViewById(R.id.buttonConfirm);
        buttonConfirm.setOnClickListener(this);
        Button buttonSave = (Button) findViewById(R.id.buttonSave);
        buttonSave.setOnClickListener(this);
        Button buttonClear = (Button) findViewById(R.id.buttonClear);
        buttonClear.setOnClickListener(this);
    }

    /**
     * Monitor for the click on one row of the ListView
     */
    @Override
    protected void onListItemClick(ListView l, View v, int position, long id) {

        ViewHolder tmpHolder = (ViewHolder) v.getTag();    // get the tag for this element
        String name = tmpHolder.fullName;
        Toast.makeText(this, name, Toast.LENGTH_LONG).show();
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.buttonConfirm:
                if (DataCache.getInstance().getOrder() != null
                        && DataCache.getInstance().getOrder().getFoodList() != null
                        && !DataCache.getInstance().getOrder().getFoodList().isEmpty()) {
                    Intent intent = new Intent(this, OrderRoute.class);
                    startActivityForResult(intent, ORDER_NAVIGATION);
                } else {
                    Toast.makeText(this, this.getString(R.string.text_order_empty), Toast.LENGTH_LONG).show();
                }
                break;
            case R.id.buttonSave:
            /*
			 * Save order to draft
			 */
                if (DataCache.getInstance().getOrder() != null
                        && DataCache.getInstance().getOrder().getFoodList() != null
                        && !DataCache.getInstance().getOrder().getFoodList().isEmpty()) {
				/*
				 * Clear current draft if exists
				 */
                    DatabaseHelper.getInstance(this).deleteDraftOrder();
				/*
				 * Save new draft
				 */
                    DatabaseHelper.getInstance(this).insertOrderSent(DataCache.getInstance().getOrder());
                }
                break;
            case R.id.buttonClear:
                DataCache.getInstance().clearCurrentOrder();
                adapter.clear();
                adapter.notifyDataSetChanged();
                break;
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        switch (requestCode) {
            case (ORDER_NAVIGATION):
                if (resultCode == Activity.RESULT_OK) {
                    Intent resultIntent = new Intent();
                    setResult(Activity.RESULT_OK, resultIntent);
                    finish();
                }
                break;
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.order_overview, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        Intent intent;
        switch (item.getItemId()) {
            case R.id.action_order_promotion:
                intent = new Intent(this, PromotionList.class);
                startActivityForResult(intent, ORDER_NAVIGATION);
                return true;
            case R.id.action_order_menu:
                intent = new Intent(this, MenuList.class);
                intent.putExtra("menuLevel", 0);
                intent.putExtra("supermenuId", 0);
                startActivityForResult(intent, ORDER_NAVIGATION);
                return true;
            case android.R.id.home:
                Intent resultIntent = new Intent();
                setResult(Activity.RESULT_OK, resultIntent);
                finish();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }
}
