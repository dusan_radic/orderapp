package com.hyperether.orderapp.user;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;

import com.hyperether.orderapp.R;
import com.hyperether.orderapp.components.DataCache;
import com.hyperether.orderapp.components.Order;
import com.hyperether.orderapp.utils.Preferences;

public class OrderRoute extends Activity implements OnClickListener{
	private static final int ORDER_NAVIGATION = 1;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_order_route);
		initLayout();
	}

	@Override
	public void onClick(View v) {
		Intent intent;
		intent = new Intent(this, Reservation.class);
		switch (v.getId()){
		case R.id.buttonPickUp:
			DataCache.getInstance().getOrder().setOrderType(Order.ORDER_FOOD_PICKUP);
			break;
		case R.id.buttonDeliver:
			DataCache.getInstance().getOrder().setOrderType(Order.ORDER_FOOD_DELIVERY);
			break;
		case R.id.buttonTableReservation:
			DataCache.getInstance().getOrder().setOrderType(Order.ORDER_FOOD_ONSITE);
			break;
		}
		startActivityForResult(intent, ORDER_NAVIGATION);
	}

	@Override
	public void onActivityResult(int requestCode, int resultCode, Intent data) {
		super.onActivityResult(requestCode, resultCode, data);
		switch(requestCode) {
		case (ORDER_NAVIGATION) :
			if (resultCode == Activity.RESULT_OK) {
				Intent resultIntent = new Intent();
				setResult(Activity.RESULT_OK, resultIntent);
				finish();
			}
		break;
		}
	}

	/**
	 * Method that initializes the main menu button layout
	 * 
	 */
	private void initLayout() {
		/*
		 * Capture our buttons from layout
		 * Register the onClick listener with the implementation above
		 */
		Button mButtonPickUp, mButtonDeliver, mButtonReservation;

		mButtonPickUp = (Button) findViewById(R.id.buttonPickUp);
		mButtonDeliver = (Button) findViewById(R.id.buttonDeliver);
		mButtonReservation = (Button) findViewById(R.id.buttonTableReservation);

		int usefulDisplayHeight = Preferences.getDisplayHeight(this);
		int dp1 = (int) (getResources().getDimension(R.dimen.activity_vertical_margin) / getResources().getDisplayMetrics().density);
		int dp2 = (int) (getResources().getDimension(R.dimen.activity_order_route_margin) / getResources().getDisplayMetrics().density);
		int buttonRowHeight = (usefulDisplayHeight - 2*dp1 - 2*dp2) / 3;

		mButtonPickUp.setHeight(buttonRowHeight);
		mButtonDeliver.setHeight(buttonRowHeight);
		mButtonReservation.setHeight(buttonRowHeight);
	}
}
