package com.hyperether.orderapp.user;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Iterator;
import java.util.Locale;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.FragmentActivity;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.hyperether.orderapp.R;
import com.hyperether.orderapp.communication.SenderApi;
import com.hyperether.orderapp.components.DataCache;
import com.hyperether.orderapp.components.Food;
import com.hyperether.orderapp.components.Order;
import com.hyperether.orderapp.datetime.DatePickerFragment;
import com.hyperether.orderapp.datetime.TimePickerFragment;
import com.hyperether.orderapp.utils.NotificationSettings;
import com.hyperether.orderapp.utils.Preferences;

public class Reservation extends FragmentActivity implements OnClickListener, DatePickerFragment.TheListener, TimePickerFragment.TheListener {
	private static final int SENDING_TYPE = 1;
	private Button mButtonDate, mButtonTime, mButtonPersons, mButtonAddress;
	private Button mButtonConfirm, mButtonCancel;
	private EditText mEditTextPersons, mEditTextAddress;
	private TextView mTextViewDate, mTextViewTime;

	private int mOrderType;
	ProgressDialog mDialog;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_reservation);

		mOrderType = DataCache.getInstance().getOrder().getOrderType();
		initLayout();

		DatePickerFragment dpf = new DatePickerFragment();
		mTextViewDate.setText(dpf.getFormattedDateCurrent());
		TimePickerFragment tpf = new TimePickerFragment();
		mTextViewTime.setText(tpf.getFormattedTimeCurrent());
	}

	@Override
	public void onClick(View v) {
		mEditTextAddress.clearFocus();
		mEditTextPersons.clearFocus();

		switch (v.getId()){
		case R.id.buttonSetDate:
		case R.id.textViewShowDate:
			showDatePickerDialog(v);
			break;
		case R.id.buttonSetTime:
		case R.id.textViewShowTime:
			showTimePickerDialog(v);
			break;
		case R.id.buttonResConf:	// If button confirm is pressed
			startOrderInsertion();
			break;
		case R.id.buttonResCancel:
			finish();
			break;
		case R.id.buttonSetAddress:
			mEditTextAddress.requestFocus();
			break;
		case R.id.buttonSetPersons:
			mEditTextPersons.requestFocus();
			break;
		}
	}

	private void initLayout() {
		/*
		 * Capture our buttons from layout
		 * Register the onClick listener with the implementation above
		 */
		mButtonConfirm = (Button) findViewById(R.id.buttonResConf);
		mButtonConfirm.setOnClickListener(this);

		mButtonCancel = (Button) findViewById(R.id.buttonResCancel);
		mButtonCancel.setOnClickListener(this);

		mButtonDate = (Button) findViewById(R.id.buttonSetDate);
		mButtonDate.setOnClickListener(this);

		mButtonTime = (Button) findViewById(R.id.buttonSetTime);
		mButtonTime.setOnClickListener(this);

		mTextViewDate = (TextView) findViewById(R.id.textViewShowDate);
		mTextViewDate.setOnClickListener(this);

		mTextViewTime = (TextView) findViewById(R.id.textViewShowTime);
		mTextViewTime.setOnClickListener(this);

		mButtonAddress = (Button) findViewById(R.id.buttonSetAddress);
		mButtonAddress.setOnClickListener(this);

		mButtonPersons = (Button) findViewById(R.id.buttonSetPersons);
		mButtonPersons.setOnClickListener(this);

		mEditTextPersons = (EditText) findViewById(R.id.editInsertPersonsNo);
		mEditTextAddress = (EditText) findViewById(R.id.editInsertAddress);

		if(mOrderType == Order.ORDER_FOOD_PICKUP){
			mButtonPersons.setVisibility(View.INVISIBLE);
			mEditTextPersons.setVisibility(View.INVISIBLE);
			mButtonAddress.setVisibility(View.INVISIBLE);
			mEditTextAddress.setVisibility(View.INVISIBLE);
		}
		else if(mOrderType == Order.ORDER_FOOD_DELIVERY){
			mButtonPersons.setVisibility(View.INVISIBLE);
			mEditTextPersons.setVisibility(View.INVISIBLE);
			mButtonAddress.setVisibility(View.VISIBLE);
			mEditTextAddress.setVisibility(View.VISIBLE);
		}
		else if((mOrderType == Order.ORDER_FOOD_ONSITE) ||
				(mOrderType == Order.ORDER_RESERVATION)
				){
			mButtonPersons.setVisibility(View.VISIBLE);
			mEditTextPersons.setVisibility(View.VISIBLE);
			mButtonAddress.setVisibility(View.INVISIBLE);
			mEditTextAddress.setVisibility(View.INVISIBLE);
		}
	}

	public void showTimePickerDialog(View v) {
		DialogFragment newFragment = new TimePickerFragment();
		newFragment.show(getSupportFragmentManager(), "timePicker");
	}

	public void showDatePickerDialog(View v) {
		DialogFragment newFragment = new DatePickerFragment();
		newFragment.show(getSupportFragmentManager(), "datePicker");
	}

	/**
	 * Use the set date to fill in the text box
	 * @param date
	 */
	@Override
	public void returnDate(String date) {
		mTextViewDate.setText(date);
	}

	/**
	 * Use the set date to fill in the text box
	 */
	@Override
	public void returnTime(String time) {
		mTextViewTime.setText(time);
	}

	@Override
	public void onActivityResult(int requestCode, int resultCode, Intent data) {
		super.onActivityResult(requestCode, resultCode, data);
		switch(requestCode) {
		case (SENDING_TYPE):
			if (resultCode == Activity.RESULT_FIRST_USER) {
				startOrderInsertion();
			}
		break;
		}
	}

	private void startOrderInsertion(){
		SharedPreferences settings = getSharedPreferences(Preferences.PREF_FILE, 0);
		int mSendingType = settings.getInt("notificationMethod", -1);
		if(mSendingType < 0){
			Intent intent = new Intent(getApplicationContext(), NotificationSettings.class);
			startActivityForResult(intent, SENDING_TYPE);
		}
		else if (checkOrder()){
			confirmOrder(mSendingType);
			//Toast.makeText(this, this.getString(R.string.text_order_sent), Toast.LENGTH_LONG).show();
			Intent resultIntent = new Intent();
			Reservation.this.setResult(Activity.RESULT_OK, resultIntent);
			finish();
		}
	}

	/**
	 * Switch sending method
	 * 
	 */
	private void confirmOrder(int mSendingType) {
		SenderApi mSender = new SenderApi(this);

		switch (mSendingType) {
		case NotificationSettings.SMS:
			DataCache.getInstance().getOrder().setStatus(Order.SMS);
			mSender.sendSMS(DataCache.getInstance().getOrder().getOrderText());
			break;
		case NotificationSettings.EMAIL:
			DataCache.getInstance().getOrder().setStatus(Order.EMAIL);
			mSender.sendEmail(DataCache.getInstance().getOrder().getOrderText());
			break;
		case NotificationSettings.WEBSERVICE:
			DataCache.getInstance().getOrder().setStatus(Order.APP_PENDING);
			break;
		}

		mSender.startInsertService(mSendingType);
	}

	/**
	 * Check the user input data
	 * 
	 */
	private boolean checkOrder() {
		boolean orderValid = false;
		String message;
		Order order = DataCache.getInstance().getOrder();

		if((this.mTextViewDate.getText() != "") && (this.mTextViewTime.getText() != "")){
			String tmpDateTime = "" + mTextViewDate.getText() + " " + mTextViewTime.getText();
			try {
				Date orderDate = new SimpleDateFormat("yyyy-MM-dd HH:mm", Locale.ENGLISH).parse(tmpDateTime);
				order.setTimeRequired(orderDate);
			} catch (ParseException e) {
				e.printStackTrace();
			}

			order.setOrderTime(new Date(System.currentTimeMillis()));
			if(order.getTimeRequired() == null)
				order.setTimeRequired(new Date(System.currentTimeMillis()));

			message = order.getOrderTypeText(this);
			message += "\n" + getString(R.string.text_send_time);
			message += " " + order.getOrderTime();
			message += "\n" + getString(R.string.text_required_time);
			message += " " + order.getTimeRequired() + "\n";

			switch (this.mOrderType){
			case Order.ORDER_FOOD_PICKUP:
				message += generateFoodListText(order);
				orderValid = true;
				break;
			case Order.ORDER_FOOD_DELIVERY:
				if(mEditTextAddress.getText().toString() != ""){
					orderValid = true;
					order.setAddress(mEditTextAddress.getText().toString());
					message += getString(R.string.text_address);
					message += ": " + mEditTextAddress.getText() + "\n";
				}
				message += generateFoodListText(order);
				break;
			case Order.ORDER_FOOD_ONSITE:
				message += generateFoodListText(order);
			case Order.ORDER_RESERVATION:
				try{
					if(Integer.parseInt(mEditTextPersons.getText().toString()) > 0){
						orderValid = true;
						order.setNumberOfPeople(Integer.parseInt(mEditTextPersons.getText().toString()));
						message += "\n" + getString(R.string.text_reservation_persons);
						message += " " + mEditTextPersons.getText();
					}
				} catch (NumberFormatException ex) {
					Log.d(Preferences.LOG_TAG, "Number error");
				}
				break;
			}

			if(orderValid){
				order.setOrderText(message);
			}
		}
		return orderValid;
	}

	/**
	 * Method that generates text message contents based on the current order contents
	 * 
	 * @param order - Current order
	 * @return - message string
	 */
	private String generateFoodListText(Order order){
		String message = getString(R.string.text_food_list) + "\n";
		Iterator<Food> iterator = order.getFoodList().iterator();
		while(iterator.hasNext()) {
			Food tmpFood = iterator.next();
			message += tmpFood.getText() + " " + getString(R.string.text_food_count) + tmpFood.getCount() + "\n";
		}
		message += "\n" + getString(R.string.text_total_price) + order.getTotalPriceString(this) + "\n";
		return message;
	}

	@Override
	public void onSaveInstanceState(Bundle savedInstanceState) {
		savedInstanceState.putCharSequence("date", mTextViewDate.getText());
		savedInstanceState.putCharSequence("time", mTextViewTime.getText());
		savedInstanceState.putCharSequence("persons", mEditTextPersons.getText());
		savedInstanceState.putCharSequence("address", mEditTextAddress.getText());
		super.onSaveInstanceState(savedInstanceState);
	}

	@Override
	public void onRestoreInstanceState(Bundle savedInstanceState) {
		super.onRestoreInstanceState(savedInstanceState);
		mTextViewDate.setText(savedInstanceState.getCharSequence("date"));
		mTextViewTime.setText(savedInstanceState.getCharSequence("time"));
		mEditTextPersons.setText(savedInstanceState.getCharSequence("persons"));
		mEditTextAddress.setText(savedInstanceState.getCharSequence("address"));
	}
}
