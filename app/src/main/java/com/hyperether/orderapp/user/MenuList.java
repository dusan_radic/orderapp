package com.hyperether.orderapp.user;

import java.util.List;

import android.app.Activity;
import android.app.ListActivity;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.support.v4.content.LocalBroadcastManager;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ListView;

import com.hyperether.orderapp.R;
import com.hyperether.orderapp.adapters.CarteBaseAdapter;
import com.hyperether.orderapp.adapters.FoodListMenuBaseAdapter;
import com.hyperether.orderapp.components.Carte;
import com.hyperether.orderapp.components.DatabaseHelper;
import com.hyperether.orderapp.components.Food;
import com.hyperether.orderapp.components.FoodCountDialog;

/**
 * Class that presents restaurant menu
 *
 * @version March, 2014
 */
public class MenuList extends ListActivity {
    private static final int ORDER_NAVIGATION = 1;
    private static final int MENU_NAVIGATION = 2;
    private CarteBaseAdapter adapter;
    private FoodListMenuBaseAdapter adapterFood;
    private int mMenuLevel;
    private int mSupermenuId;
    private boolean flgFood = false;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_element_list);

        Bundle b = getIntent().getExtras();
        mSupermenuId = b.getInt("supermenuId");
        mMenuLevel = b.getInt("menuLevel");
        mMenuLevel++;

        createList();
    }

    private void createList() {
        DatabaseHelper mOpenHelper = DatabaseHelper.getInstance(this);
        List<Carte> items = mOpenHelper.getCarteList(mSupermenuId);

        ListView list = (ListView) findViewById(android.R.id.list);
        adapter = new CarteBaseAdapter(this, items);
        if (adapter.getCount() > 0) {
            list.setAdapter(adapter);
        } else {
            List<Food> items1 = mOpenHelper.getFoodListBySupermenuId(mSupermenuId);
            adapterFood = new FoodListMenuBaseAdapter(this, items1);
            list.setAdapter(adapterFood);
            flgFood = true;
        }
    }

    /**
     * Monitor for the click on one row of the ListView
     */
    @Override
    protected void onListItemClick(ListView l, View v, int position, long id) {
        if (!flgFood) {
            Carte carte = (Carte) adapter.getItem(position);
            Intent intent = new Intent(this, MenuList.class);
            intent.putExtra("menuLevel", this.mMenuLevel);
            intent.putExtra("supermenuId", carte.getId());
            startActivityForResult(intent, MENU_NAVIGATION);
        } else {
            FoodCountDialog dialog = new FoodCountDialog(this, (Food) adapterFood.getItem(position), adapterFood);
            dialog.getFoodCount();
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        switch (requestCode) {
            case (MENU_NAVIGATION):
                if (resultCode == Activity.RESULT_OK) {
                    if (mMenuLevel > 0) {
                        Intent resultIntent = new Intent();
                        setResult(Activity.RESULT_OK, resultIntent);
                        finish();
                    }
                }
                break;
            case (ORDER_NAVIGATION):
                if (resultCode == Activity.RESULT_OK) {
                    Intent resultIntent = new Intent();
                    setResult(Activity.RESULT_OK, resultIntent);
                    finish();
                }
                break;
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.order_menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        Intent intent;
        switch (item.getItemId()) {
            case R.id.action_order_overview:
                intent = new Intent(this, OrderList.class);
                startActivityForResult(intent, ORDER_NAVIGATION);
                return true;
            case R.id.action_order_promotion:
                intent = new Intent(this, PromotionList.class);
                startActivityForResult(intent, ORDER_NAVIGATION);
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    // handler for received Intents for the "order-changed" event
    private final BroadcastReceiver mMessageReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            // Refresh the list data and update adapter
            adapterFood.notifyDataSetChanged();
        }
    };

    @Override
    protected void onResume() {
        super.onResume();
        createList();
        // Register mMessageReceiver to receive messages that the current order has changed
        LocalBroadcastManager.getInstance(this).registerReceiver(mMessageReceiver,
                new IntentFilter("order-changed"));
    }

    @Override
    protected void onPause() {
        // Unregister since the activity is not visible
        LocalBroadcastManager.getInstance(this).unregisterReceiver(mMessageReceiver);
        super.onPause();
    }
}
